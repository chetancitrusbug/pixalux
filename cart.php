<?php session_start(); ?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cart : Black And White Bootstrap Landing Page / Portfolio</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="all,follow">
  <!-- Bootstrap CSS-->
  <link rel="stylesheet" href="http://pixalux.totalsimplicity.com.au/vendor/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome CSS-->
  <link rel="stylesheet" href="http://pixalux.totalsimplicity.com.au/vendor/font-awesome/css/font-awesome.min.css">
  <!-- Lightbox-->
  <link rel="stylesheet" href="http://pixalux.totalsimplicity.com.au/vendor/lightbox2/css/lightbox.min.css">
  <!-- theme stylesheet-->
  <link rel="stylesheet" href="style.default.css" id="theme-stylesheet">
  <!-- Custom stylesheet - for your changes-->
  <link rel="stylesheet" href="http://pixalux.totalsimplicity.com.au/css/custom.css">
  <!-- Favicon-->
  <link rel="shortcut icon" href="http://pixalux.totalsimplicity.com.au/img/favicon.ico">
  <!-- Tweaks for older IEs-->
  <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <style type="text/css">
    	.third{
    		overflow: hidden;
    	}
    	#SvgjsSvg1001{
    		height: 100px;
    		width: 100px;
    	}
    	.left{
    		float: left;
    	}
    	.right{
    		margin-top: 5px;
    		float: right;
    	}
    	.second_image, .first_image{
			width: 100px;
    	}
    	.second_image{
    		margin-top: 7px;
    	}
    	.third_image{
    		position: absolute;
		    clip: rect(19px,222px,111px,115px);
		    width: 223px;
		    border: 1px solid red;
		    right: 15px;
		    bottom: 2px;
    	}
    </style>
</head>

<body>
  <!-- navbar-->
  <?php 
  	$price = 28.72;
  	$ip = '192.168.1.1';
  	$_SESSION['ip'] = $ip;
  	$end = isset($_SESSION[$ip])?array_keys($_SESSION[$ip]):[];
  	$index = isset($_SESSION[$ip])?end($end)+1:0;
  	// echo "<pre>"; print_r($_POST); exit();
  	if(!empty($_POST)){
  		$_SESSION['error'] = [];
  		if(!isset($_POST['led']) || empty($_POST['led'])){
  			$_SESSION['error']['led'] = 'Please select your LEDs';
  		}
  		if(!isset($_POST['side']) || empty($_POST['side'])){
  			$_SESSION['error']['side'] = 'Please choose how many Faces are lit';
  		}
  		if(empty($_POST['profile_name'])){
  			$_SESSION['error']['profile'] = 'Please Choose how many Faces are lit';
  		}
  		if(empty($_POST['frame_color'])){
  			$_SESSION['error']['frame_color'] = 'Please Choose your frame color';
  		}
  		if(!isset($_POST['cexit']) || empty($_POST['cexit'])){
  			$_SESSION['error']['cexit'] = 'Please choose your cable exit';
  		}
  		if(!isset($_POST['chexit']) || empty($_POST['chexit'])){
  			$_SESSION['error']['chexit'] = 'Please choose your cable exit';
  		}
  		if(empty($_SESSION['error'])){
  			// echo "<pre>"; print_r($_POST); exit();
  			if(isset($_POST['key']) && $_POST['key'] >= 0 && $_POST['key'] != ''){
  				// $_POST['third_part'] = base64_encode($_POST['third_part']);
		  		$_POST['mycp'] = $_POST['frame_color'];
		  		$_SESSION[$ip][$_POST['key']] = $_POST;
		  	}else if(!empty($_POST['led_name'])){
		  		// $_POST['third_part'] = base64_encode($_POST['third_part']);
		  		$_POST['mycp'] = $_POST['frame_color'];
		  		$_SESSION[$ip][$index] = $_POST;
		  	}
  		}else{
  			if(isset($_POST['key']) && $_POST['key'] >= 0 && $_POST['key'] != ''){
  				// header('localhost:82?id='.$_POST['key']);	
  				header('http://pixalux.totalsimplicity.com.au?id='.$_POST['key']);	
  			}else{
  				// header('http://localhost:82');	
  				header('http://pixalux.totalsimplicity.com.au');
  			}
  		}
  	}
	
  	// echo "<pre>"; print_r($_SESSION); exit();
  	if($_GET['load'] != 1){
	    header('Location:cart.php?load=1');
	}
	include_once 'api/config/database.php';
	include_once 'api/objects/drawing.php';
	 
	// get database connection
	$database = new Database();
	$db = $database->getConnection();
	 
	// prepare drawing object
	$drawing = new drawing($db);
  ?>
  <header class="header">
    <nav class="navbar navbar-expand-lg">
      <div class="container">
        <a href="#intro" class="navbar-brand link-scroll">
          <img src="http://pixalux.totalsimplicity.com.au/img/logo.png" alt="" class="img-fluid">
        </a>
        <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
          aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right">
          <i class="fa fa-bars"></i>
        </button>
        <div id="navbarSupportedContent" class="collapse navbar-collapse">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a href="#intro" class="nav-link link-scroll">Home</a>
            </li>
            <li class="nav-item">
              <a href="#about" class="nav-link link-scroll">About </a>
            </li>
            <li class="nav-item">
              <a href="#services" class="nav-link link-scroll">Services</a>
            </li>
            <li class="nav-item">
              <a href="#portfolio" class="nav-link link-scroll">Portfolio</a>
            </li>
            <li class="nav-item">
              <a href="#text" class="nav-link link-scroll">Text</a>
            </li>
            <li class="nav-item">
              <a href="#contact" class="nav-link link-scroll">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  
   <!-- cart-->
  	<section id="cart" class="text">
    	<div class="container">
      		<div class="row">
        		<div class="col-sm-12">
          			<h1>Shopping Cart</h1>
          			<p class="section-subtitle">Choose the way you want</p>
        		</div>
      		</div>
      		<div class="row">
				<div class="cart-grid-body col-xs-12 col-lg-8 col-sm-12 col-md-8">
					
					<?php
					if(isset($_SESSION[$ip]) && !empty($_SESSION[$ip])){
						foreach ($_SESSION[$ip] as $key => $value) {
					?>
							<div class="cart-box clearfix data-<?php echo $key; ?>">
							<!-- cart products detailed -->
								<div class="cart cart-container">
									<div class="product-line-grid clearfix">
										<!--  product left content: image-->
									  <div class="product-line-grid-left col-md-4 col-xs-4 left">
											<span class="product-image media-middle" style="position: relative;">
												<div class="left">
													<img src="<?php echo $value['first_part']; ?>" class="first_image"><br/>
													<img src="<?php echo $value['second_part']; ?>" class="second_image">
												</div>
												<div class="right">
													<img src="<?php echo $value['third_part']; ?>" class="third_image">
												</div>
												<div style="clear: both;"></div>
											</span>
									  </div>
									  <!--  product left body: description -->
									  <div class="product-line-grid-body col-md-4 col-xs-8 mid">
											<div class="product-line-info">
											  <a class="product-name" href="#" data-id_customization="0">Product Name</a>
											</div>
											<div class="product-line-info">
												<span class="label">Size:</span>
												<span class="value"><?php echo $value['length'].'mm x '.$value['depth'].'mm'; ?></span>								  
											</div>
											<div class="product-line-info">
												<span class="label">LEDs:</span>
												<span class="value"><?php echo $value['led_name']; ?></span>
											</div>
											<div class="product-line-info">
												<span class="label">Faces are lit:</span>
												<span class="value"><?php echo $value['lit_name']; ?></span>
											 </div>
											<div class="product-line-info">
												<span class="label">Profile:</span>
												<span class="value"><?php
													// set ID property of drawing to be edited
													$drawing->id = $value['profile_name'];
													$data = $drawing->readOne(true);
													echo $data['name'];
												?></span>
											</div>
											<div class="product-line-info">
												<span class="label">Frame Colour:</span>
												<span class="value"><?php
													if($value['mycp'] == '#fff'){
														echo 'White';
													}elseif('#00'){
														echo 'Black';
													}else{
														echo 'Grey';
													}
												?></span>
											</div>
											<div class="product-line-info">
												<span class="label">Cable Exit:</span>
												<span class="value"><?php echo $value['cable_side']; ?> | Exit: <?php echo $value['cable_exit']; ?></span>
											</div>
										</div>

									  	<!--  product left body: description -->
									  	<div class="product-line-grid-right product-line-actions col-md-4 col-xs-12 right price_box-<?php echo $key; ?>">
											<div class="row">
										  		<div class="col-md-12 col-xs-12">
													<div class="row">
											  			<div class="col-md-7 col-xs-7 qty">
														 	<form class="form-horizontal" role="form">
														  		<div class="input-group bootstrap-touchspin">
																	<div class="form-group">
																		<input id="qty" type="text" value="1" data-key="<?php echo $key; ?>" name="qty" class="qty col-md-8 form-control qty-<?php echo $key; ?>">
																	</div>
																</div>

																<p><a href="#" class="btn btn-remove remove-from-cart" data-key="<?php echo $key; ?>">Remove</a></p>
																<!-- <p><a href="http://localhost:82?id=<?php echo $key; ?>" class="btn btn-edit">Edit</a></p> -->
																<p><a href="http://pixalux.totalsimplicity.com.au?id=<?php echo $key; ?>" class="btn btn-edit">Edit</a></p>
															</form>
													  	</div>
											  			<div class="col-md-5 col-xs-5 price">
																<span class="product-price">
												  				<strong>$<span class="price-<?php echo $key; ?>">28.72</span></strong>
												  				<span class="value"></span>
																</span>
											  			</div>
													</div>
									  			</div>
											</div>
								  		</div>
								  	<div class="clearfix"></div>
									</div><!-- end of product-line-grid -->
								</div><!-- end of cart cart-container -->
							</div><!-- end of cart-box -->
					<?php	
						}
					}
					?>
				
					<div class="clearfix"><a class="btn btn-shopping" href="http://pixalux.totalsimplicity.com.au">Continue shopping</a></div>
			
				</div><!-- end of col -->
		
				<div class="cart-grid-right col-xs-12 col-lg-4 col-sm-12 col-md-4">
					<div class="right-box">
						<div class="card cart-summary">
							<div class="cart-detailed-totals clearfix">
								<form action="checkout.php" method="POST">
									<div class="price_loop">
									<?php
									// To get qty and price in checkout page
									if(isset($_SESSION[$ip]) && !empty($_SESSION[$ip])){
										foreach ($_SESSION[$ip] as $key => $value) {
									?>
										<input type="hidden" name="qty[<?php echo $key; ?>]" data-tag='qty' class="qty-<?php echo $key; ?>" value="1">
										<input type="hidden" name="price[<?php echo $key; ?>]" data-tag='price' class="price-<?php echo $key; ?>" value="<?php echo $price; ?>">
									<?php
										}
									}
									?>
									</div>
									<div class="card-block">
										<div class="cart-summary-line" id="cart-subtotal-products">
											<span class="label js-subtotal"><span class="total_qty_disp">1</span> item</span>
											<span class="value">$<span class="total_price_disp"> 28.72</span></span>
											<input type="hidden" name="item" class="total_qty_disp" value="1">
											<input type="hidden" name="item_price" class="total_price_disp" value="28.72">
										</div>
										<div class="cart-summary-line" id="cart-subtotal-shipping">
											<span class="label">Shipping</span>
											<span class="value">$<span class="shipping_charge">7.00</span></span>
											<input type="hidden" name="shipping" value="7">
											<div><small class="value"></small></div>
										</div>
									</div>

									<div class="block-promo">
										<div class="cart-voucher">
											<p>
												<a class="collapse-button promo-code-button" data-toggle="collapse" href="#promo-code" aria-expanded="false" aria-controls="promo-code">Have a promo code?</a>
											</p>
								
											<div class="promo-code collapse" id="promo-code">
												<!-- <form action="#" data-link-action="add-voucher" method="post"> -->
											  	<input name="token" value="" type="hidden">
											  	<input name="addDiscount" value="1" type="hidden">
											  	<input class="form-control promo-input" name="discount_name" placeholder="Promo code" type="text">
											  	<button type="submit" class="btn btn-primary"><span>Add</span></button>
												<!-- </form> -->
											</div>
										</div>
									</div>
									<hr>
									<div class="card-block">
										<div class="cart-summary-line cart-total">
										  <span class="label">Total (tax excl.)</span>
										  <span class="value">$<span class="total_price_text">35.72</span>
										  <input type="hidden" name="total_price" value="" class="total_price_text">
										</div>

										<div class="cart-summary-line">
										  <small class="label">Taxes</small>
										  <small class="value">$0.00</small>
										</div>
					  				</div>
							  		<hr>
									</div><!-- end of cart-detailed-totals -->
									<div class="checkout cart-detailed-actions card-block">
										<div class="text-xs-center">
											<button type="submit" class="btn black-btn">Checkout</button>				  
										</div>
									</div>
								</form>
							<div id="block-reassurance">
								<ul>
									<li>
									  <div class="block-reassurance-item">
											<span class="reassurance-img">
												<img src="ic_verified_user_black_36dp_1x.png" alt="Security policy (edit with Customer reassurance module)">
											</span>
											<span>Security policy</span>
									  </div>
									</li>
									<li>
									  <div class="block-reassurance-item">
											<span class="reassurance-img">
												<img src="ic_local_shipping_black_36dp_1x.png" alt="Delivery policy (edit with Customer reassurance module)">
											</span>
											<span>Delivery policy</span>
									  </div>
									</li>
						  		<li>
							  		<div class="block-reassurance-item">
										<span class="reassurance-img">
											<img src="ic_swap_horiz_black_36dp_1x.png" alt="Return policy (edit with Customer reassurance module)">
										</span>
										<span>Return policy</span>
							  		</div>
								</li>
					  		</ul>
			  			</div><!-- end of block-reassurance -->
						</div><!-- end of card cart-summary -->	
					</div><!-- end of right-box -->
				</div><!-- end of col -->
			</div><!-- end of row -->
		</div><!-- end of container -->
	</section><!-- end of cart -->
  <footer style="background-color: #98999A;">
    <div class="container">
      <div class="row copyright">
        <div class="col-md-6">
          <p class="mb-md-0 text-center text-md-left">&copy;2018 PIXALUX MANUFACTURING AUSTRLIA</p>
        </div>
        <div class="col-md-6">
          <p class="credit mb-md-0 text-center text-md-right">Created by
            <a href="https://www.Leapfrogmarket.com.au">Leapfrog Market</a>
          </p>
        </div>
      </div>
    </div>
  </footer>
  <!-- JavaScript files-->
  <script src="http://pixalux.totalsimplicity.com.au/vendor/jquery/jquery.min.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/popper.js/umd/popper.min.js">
  </script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/jquery.cookie/jquery.cookie.js">
  </script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/jquery.cookie/jquery.cookie.js">
  </script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/lightbox2/js/lightbox.min.js"></script>
  </script>
  <script src="front.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/js/svg.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/app/app.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/app/designs/read-designs.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/app/products/create-product.js"></script>
  
  
  <link href="jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" media="all">
   <script src="jquery.bootstrap-touchspin.js"></script>
   <script src="app/cart.js"></script>
  	<script>
	    $("input[name='qty']").TouchSpin({
	        min: 1,
	        max: 1000,
	        stepinterval: 50,
	        maxboostedstep: 100
	    });
  	</script>
  
  	<script>
		jQuery(document).ready(function($) {
			$('.remove-from-cart').on('click',function(){
				$key = $(this).data('key');
				$.ajax({
					url: 'session.php',
					type: 'POST',
					data: {key: $key},
				})
				.done(function(data) {
					$('.data-'+$key).remove();
				})
				.fail(function() {
					console.log("error");
				});
			});

	            $("#SvgjsSvg1001").attr({
	            	viewBox: "0 0 500 500"
	            });
			setTimeout(function() {
	        }, 10);
		});

  	</script>
  <?php
  ?>
</body>

</html>