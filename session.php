<?php session_start();

$key = $_POST['key'];

if(array_key_exists($key, $_SESSION[$_SESSION['ip']])){
	unset($_SESSION[$_SESSION['ip']][$key]);
	return 'success';
}else{
	return false;
}