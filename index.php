<?php session_start(); $ip = '192.168.1.1'; ?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Black And White Bootstrap Landing Page / Portfolio</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="all,follow">
  <!-- Bootstrap CSS-->
  <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome CSS-->
  <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
  <!-- Lightbox-->
  <link rel="stylesheet" href="vendor/lightbox2/css/lightbox.min.css">
  <!-- theme stylesheet-->
  <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
  <!-- Custom stylesheet - for your changes-->
  <link rel="stylesheet" href="css/custom.css">
  <!-- Favicon-->
  <link rel="shortcut icon" href="img/favicon.ico">
  <style type="text/css">
    .highlight{
      color: #fff !important;
      background-color: #6c757d !important;
      border-color: #6c757d !important;
    }
    .error{
      color: red;
    }
    .color-highlight{
      border: 5px solid #6c757d !important;
    }
    #success-alert{
      display: none;
    }
    .border{
      border:3px solid #6c757d !important;
    }
  </style>
  <!-- Tweaks for older IEs-->
  <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>

<body>
  <!--<section id="intro" class="intro">
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col">
            <p>1741 Sydney Rd, Campbellfield, VIC, 3061</p>
          </div>
          <div class="col">
            <p class='text-right'>1300 113 532</p>
          </div>
        </div>
      </div>
  </section>
  <!-- intro end-->
  <!-- navbar-->
  <header class="header">
    <nav class="navbar navbar-expand-lg">
      <div class="container">
        <a href="#intro" class="navbar-brand link-scroll">
          <img src="img/logo.png" alt="" class="img-fluid">
        </a>
        <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
          aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right">
          <i class="fa fa-bars"></i>
        </button>
        <div id="navbarSupportedContent" class="collapse navbar-collapse">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a href="#intro" class="nav-link link-scroll">Home</a>
            </li>
            <li class="nav-item">
              <a href="#about" class="nav-link link-scroll">About </a>
            </li>
            <li class="nav-item">
              <a href="#services" class="nav-link link-scroll">Services</a>
            </li>
            <li class="nav-item">
              <a href="#portfolio" class="nav-link link-scroll">Portfolio</a>
            </li>
            <li class="nav-item">
              <a href="#text" class="nav-link link-scroll">Text</a>
            </li>
            <li class="nav-item">
              <a href="#contact" class="nav-link link-scroll">Contact</a>
            </li>
            <li class="nav-item">
              <a href="cart.php" class="nav-link link-scroll"><img src="cart.png" height="30" width="30"></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <!-- about-->
  <section id="about" class="text" style="background-color: #dae0e5;">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <?php
            if(isset($_SESSION['error']) && !empty($_SESSION['error'])){
              foreach ($_SESSION['error'] as $value) {
          ?>
                <p class="section-subtitle error"><?php echo $value; ?></p>  
          <?php
              }
            }
            unset($_SESSION['error']);
          ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <h1>Create your own</h1>
          <p class="section-subtitle">
            Choose the way you want
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <!-- id="create-product-form" -->
          <?php
            if(isset($_GET['id']) && isset($_SESSION[$ip][$_GET['id']])){
              $sessionData = $_SESSION[$ip][$_GET['id']];
              // echo "<pre>"; print_r($sessionData); exit();
            }
          ?>
          <form action="cart.php" method="POST" id="submit_form">
            <input type="hidden" name="key" value="<?php echo isset($_GET['id'])?$_GET['id']:null; ?>">
            <input type="hidden" name="led_name" value="<?php echo isset($sessionData['led_name'])?$sessionData['led_name']:null ?>">
            <input type="hidden" name="lit_name" value="<?php echo isset($sessionData['lit_name'])?$sessionData['lit_name']:null ?>">
            <input type="hidden" name="profile_name" value="<?php echo isset($sessionData['profile_name'])?$sessionData['profile_name']:null ?>">
            <input type="hidden" name="cable_side" value="<?php echo isset($sessionData['cable_side'])?$sessionData['cable_side']:null ?>">
            <input type="hidden" name="cable_exit" value="<?php echo isset($sessionData['cable_exit'])?$sessionData['cable_exit']:null ?>">
            <span class="led_id" data-id="<?php echo isset($sessionData['led'])?$sessionData['led']:null ?>">
            <span class="side_id" data-id="<?php echo isset($sessionData['side'])?$sessionData['side']:null ?>">
            <span class="cexit_id" data-id="<?php echo isset($sessionData['cexit'])?$sessionData['cexit']:null ?>">
            <span class="chexit_id" data-id="<?php echo isset($sessionData['chexit'])?$sessionData['chexit']:null ?>">
            <div id="accordion">
              <div class="card bg-light">
                <div class="card-header" id="headingFour">
                  <i class="fa fa-arrows fa-fw"></i>
                  <a data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                    1. Choose your size
                  </a>
                </div>
                <div id="collapseFour" class="collapse show" aria-labelledby="headingFour" data-parent="#accordion">
                  <div class="card-block p-3" id="">
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="length">Length</label>
                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <i class="fa fa-arrows-h"></i>
                            </div>
                          </div>
                          <input type="number" class="form-control" id="length" name="length" min=208 max=1800 placeholder="Length" value="<?php echo isset($sessionData['length'])?$sessionData['length']:208 ?>">
                        </div>
                      </div>
                      <div class="form-group col-md-6">
                        <label for="depth">Depth</label>
                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <i class="fa fa-arrows-v"></i>
                            </div>
                          </div>
                          <input type="number" class="form-control" id="depth" name="depth" min=208 max=800 placeholder="Depth" value="<?php echo isset($sessionData['depth'])?$sessionData['depth']:208 ?>">
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-1">
                        <i class="fa fa-info-circle fa-2x"></i>
                      </div>
                      <div class="message form-group col-md-11">
                        <ul>
                          <div class="message" id='mintext'>
                            <li>Panels cannot be less than 208mm x 208mm.</li>
                          </div>
                          <div class="message" id='lsizetext'></div>
                          <div class="message" id='dsizetext'></div>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card bg-light">
                <div class="card-header" id="headingOne">
                  <i class="fa fa-lightbulb-o fa-fw"></i>
                  <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    2. Choose your LEDs
                  </a>
                </div>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-block p-3" id="">
                    <div class="form-row">
                      <div class="form-group col-md-12">
                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <i class="fa fa-lightbulb-o"></i>
                            </div>
                          </div>
                          <select id="led" name="led" class="custom-select">
                            <option></option>
                          </select>
                          <input type="text" id="colour" name="colour" value="<?php echo isset($sessionData['colour'])?$sessionData['colour']:null ?>" hidden>
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-1">
                        <i class="fa fa-info-circle fa-2x"></i>
                      </div>
                      <div class="message form-group col-md-11">
                        <ul>
                          <li>
                            <div class="message" id='pitchtext'>Choosing the LED will set the pitch.</div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card bg-light">
                <div class="card-header" id="headingTwo">
                  <i class="fa fa-sun-o fa-fw"></i>
                  <a data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                    3. Choose how many Faces are lit
                  </a>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                  <div class="card-body">
                    <div class="input-group">
                      <label class="sr-only" for="side">Sides</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <div class="input-group-text">
                            <i class="fa fa-lightbulb-o"></i>
                          </div>
                        </div>
                        <select id="side" name="side" class="custom-select">
                          <option></option>
                        </select>
                      </div>
                      <div class="message" id='sidetext'>Choosing the LED will set the pitch.</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card bg-light">
                <div class="card-header" id="headingThree">
                  <i class="fa fa-square-o fa-fw"></i>
                  <a data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                    4. Choose your profile
                  </a>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                  <div class="card-block pb-3" id="profileDrawing">
                  </div>
                </div>
              </div>
              <div class="card bg-light">
                <div class="card-header" id="headingFive">
                  <i class="fa fa-paint-brush fa-fw"></i>
                  <a data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                    5. Choose your Frame Colour
                  </a>
                </div>
                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                  <div class="card-block p-3" id="">
                    <div class="form-row">
                      <div class="form-group col-md-12">
                        <label for="mycp">Finish colour</label>
                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <i class="fa fa-paint-brush"></i>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <input type="text" class="form-control" id="mycp1" name="mycp" value="#000" style="background-color:#000;">
                          </div>
                          <div class="col-sm-3">
                            <input type="text" class="form-control" id="mycp2" name="mycp" value="#666" style="background-color:#666;">
                          </div>
                          <div class="col-sm-3">
                            <input type="text" class="form-control" id="mycp3" name="mycp" value="#fff" style="background-color:#fff;">
                          </div>
                        </div>
                        <input type="hidden" name="frame_color" value="<?php echo isset($sessionData['frame_color'])?$sessionData['frame_color']:null ?>">
                        <div class="message" id='sidetext'>Selected colour applies to the colour of the profiles as well as the 2mm ABS edge banding on the sides that doe not get a profile.</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card bg-light">
                <div class="card-header" id="headingSix">
                  <i class="fa fa-external-link fa-fw"></i>
                  <a data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                    6. Choose your Cable Exit
                  </a>
                </div>
                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                  <div class="card-block p-3" id="">
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="cexit">Choose Side</label>
                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <i class="fa fa-external-link"></i>
                            </div>
                          </div>
                          <select id="cexit" name="cexit" class="form-control">
                            <option></option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group col-md-6">
                        <label for="chexit">Cable exit</label>
                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <i class="fa fa-external-link"></i>
                            </div>
                          </div>
                          <select id="chexit" name="chexit" class="form-control">
                            <option selected="true" disabled>choose</option>
                            <option>Left</option>
                            <option>Right</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="input-group mb-2" hidden>
              <div class="form-group col-md-6">
                <label for="pitch">Pitch</label>
                <div class="input-group mb-2">
                  <input class="form-control" type="text" id="pitch" name="pitch" value="<?php echo isset($sessionData['pitch'])?$sessionData['pitch']:null ?>">
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="height">height</label>
                <div class="input-group mb-2">
                  <input class="form-control" type="text" id="height" name="height" value="<?php echo isset($sessionData['height'])?$sessionData['height']:null ?>">
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="LineType">LineType</label>
                <div class="input-group mb-2">
                  <input class="form-control" type="text" id="LineType" name="LineType" value="<?php echo isset($sessionData['LineType'])?$sessionData['LineType']:null ?>">
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="newlength">newlength</label>
                <div class="input-group mb-2">
                  <input class="form-control" type="text" id="newlength" name="newlength" value="<?php echo isset($sessionData['newlength'])?$sessionData['newlength']:null ?>">
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="newdepth">newdepth</label>
                <div class="input-group mb-2">
                  <input class="form-control" type="text" id="newdepth" value=800 name="newdepth" value="<?php echo isset($sessionData['newdepth'])?$sessionData['newdepth']:null ?>">
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="edge">edge</label>
                <div class="input-group mb-2">
                  <input class="form-control" type="text" id="edge" name="edge" value="<?php echo isset($sessionData['edge'])?$sessionData['edge']:null ?>">
                </div>
              </div>
            </div>
            <canvas id="canvas1" style="display: none;"></canvas>
            <canvas id="canvas2" style="display: none;"></canvas>
            <canvas id="canvas3" style="display: none;"></canvas>
            <input type="hidden" name="first_part" value="<?php echo isset($sessionData['first_part'])?$sessionData['first_part']:null ?>"></canvas>
            <input type="hidden" name="second_part" value="<?php echo isset($sessionData['second_part'])?$sessionData['second_part']:null ?>"></canvas>
            <input type="hidden" name="third_part" value="<?php echo isset($sessionData['third_part'])?$sessionData['third_part']:null ?>"></canvas>
            <button type="submit" class="btn btn-primary cart_button">Add to Cart</button>
          </form>
        </div>
        <div class="col-md-3">
          <h4 class="text-center">Profile Preview</h4>
          <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 40 275 200" id="first_part" class="first_part">
              <g id="profileCutTop" transform="translate(272 280) rotate(180)">
                <g id="topledcolor"></g>
                <g id="topbase"></g>
                <g id="toppoly"></g>
                <g id="topcable"></g>
              </g>
          </svg>
          <svg id="profileCutBottom" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 40 275 200" class="second_part">
              <g id="botledcolor"></g>
              <g id="botbase"></g>
              <g id="botpoly"></g>
              <g id="botcable"></g>
          </svg>
        </div>
        <div class="col-md-5" id="PixaluxDrawing">
          <h4 class="text-center">Size Ratio
            <br>Preview</h4>
              <div id="pixalux" class="third_part">
              </div>
          <canvas id="canvas1" name='first_part' style="display:none">
          <canvas id="canvas2" name='second_part' style="display:none">
          <canvas id="canvas3" name='third_part' style="display:none">
          <div class="alert alert-success" id="success-alert">
            <strong>Success! </strong>
            Product is added to your cart.
        </div>
              </div>
      </div>
  </section>
  <!-- contact-->
  <section id="contact" style="background-color: #fff;" class="text-page">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="heading">Contact</h2>
          <div class="row">
            <div class="col-lg-6">
              <form id="contact-form" method="post" action="#" class="contact-form">
                <div class="controls">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="name">Your firstname *</label>
                        <input type="text" name="name" placeholder="Enter your firstname" required="required" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="surname">Your lastname *</label>
                        <input type="text" name="surname" placeholder="Enter your  lastname" required="required" class="form-control">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="surname">Your email *</label>
                    <input type="email" name="email" placeholder="Enter your  email" required="required" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="surname">Your message for us *</label>
                    <textarea rows="4" name="message" placeholder="Enter your message" required="required" class="form-control"></textarea>
                  </div>
                  <div class="text-center">
                    <input type="submit" name="name" value="Send message" class="btn btn-outline-primary btn-block">
                  </div>
                </div>
              </form>
            </div>
            <div class="col-lg-6">
              <p>Effects present letters inquiry no an removed or friends. Desire behind latter me though in. Supposing shameless
                am he engrossed up additions. My possible peculiar together to. Desire so better am cannot he up before points.
                Remember mistaken opinions it pleasure of debating. Court front maids forty if aware their at. Chicken use
                are pressed removed. </p>
              <p>Able an hope of body. Any nay shyness article matters own removal nothing his forming. Gay own additions education
                satisfied the perpetual. If he cause manor happy. Without farther she exposed saw man led. Along on happy
                could cease green oh. </p>
              <p class="social">
                <a href="#" title="" class="facebook">
                  <i class="fa fa-facebook"></i>
                </a>
                <a href="#" title="" class="twitter">
                  <i class="fa fa-twitter"></i>
                </a>
                <a href="#" title="" class="gplus">
                  <i class="fa fa-google-plus"></i>
                </a>
                <a href="#" title="" class="instagram">
                  <i class="fa fa-instagram"></i>
                </a>
                <a href="#" title="" class="email">
                  <i class="fa fa-envelope"></i>
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <footer style="background-color: #98999A;">
    <div class="container">
      <div class="row copyright">
        <div class="col-md-6">
          <p class="mb-md-0 text-center text-md-left">&copy;2018 PIXALUX MANUFACTURING AUSTRLIA</p>
        </div>
        <div class="col-md-6">
          <p class="credit mb-md-0 text-center text-md-right">Created by
            <a href="https://www.Leapfrogmarket.com.au">Leapfrog Market</a>
          </p>
        </div>
      </div>
    </div>
  </footer>
  <!-- JavaScript files-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/popper.js/umd/popper.min.js">
  </script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="vendor/jquery.cookie/jquery.cookie.js">
  </script>
  <script src="vendor/jquery.cookie/jquery.cookie.js">
  </script>
  <script src="vendor/lightbox2/js/lightbox.min.js"></script>
  </script>
  <script src="js/front.js"></script>
  <script src="js/svg.js"></script>
  <script src="app/app.js"></script>
  <script src="app/designs/read-designs.js"></script>
  <script src="app/products/create-product.js"></script>
  <!-- <script src="app/home.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
  <script src="app/home_new.js"></script>
</body>

</html>