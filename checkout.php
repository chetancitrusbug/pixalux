<?php session_start(); ?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Checkout : Black And White Bootstrap Landing Page / Portfolio</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="all,follow">
  <!-- Bootstrap CSS-->
  <link rel="stylesheet" href="http://pixalux.totalsimplicity.com.au/vendor/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome CSS-->
  <link rel="stylesheet" href="http://pixalux.totalsimplicity.com.au/vendor/font-awesome/css/font-awesome.min.css">
  <!-- Lightbox-->
  <link rel="stylesheet" href="http://pixalux.totalsimplicity.com.au/vendor/lightbox2/css/lightbox.min.css">
  <!-- theme stylesheet-->
  <link rel="stylesheet" href="style.default.css" id="theme-stylesheet">
  <!-- Custom stylesheet - for your changes-->
  <link rel="stylesheet" href="http://pixalux.totalsimplicity.com.au/css/custom.css">
  <!-- Favicon-->
  <link rel="shortcut icon" href="http://pixalux.totalsimplicity.com.au/img/favicon.ico">
  <!-- Tweaks for older IEs-->
  <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>

<body>
	<?php
		// $_SESSION['POST'] = $_POST;
		include_once 'api/config/database.php';
		// get database connection
		// date_default_timezone_set('Asia/Kolkata');
		// echo "<pre>"; print_r('here'); exit();
		if(!empty($_POST)){
			$_SESSION['cart'] = $_POST;
			$db = new Database();
			// Create connection
			$conn = new mysqli($db->host, $db->username, $db->password, $db->db_name);

			// Check connection
			if ($conn->connect_error) {
			    die("Connection failed: " . $conn->connect_error);
			} 

			$sql = "INSERT INTO
	                    cart (item,item_price,shipping,total_price,deleted,created_at)
	                VALUES
	                    (".$_POST['item'].",'".$_POST['item_price']."','".$_POST['shipping']."','".$_POST['total_price']."','0',now())";

	        if ($conn->query($sql) === TRUE) {
			    $lastId = $conn->insert_id;
			    $_SESSION['created_at'] = date('F d, Y H:i:s A',time());

			    if(!empty($_SESSION[$_SESSION['ip']])){
			    	foreach ($_SESSION[$_SESSION['ip']] as $key => $value) {
			    		$sqlItem = "INSERT INTO
					                    cart_items (cart_id,led_name,lit_name,profile_name,cable_side,cable_exit,length,depth,led,colour,side,mycp,cexit,chexit,pitch,height,LineType,newlength,newdepth,edge,qty,price,deleted,created_at)
					                VALUES
					                    (".$lastId.",'".$value['led_name']."','".$value['lit_name']."','".$value['profile_name']."','".$value['cable_side']."','".$value['cable_exit']."','".$value['length']."','".$value['depth']."','".$value['led']."','".$value['colour']."','".$value['side']."','".$value['mycp']."','".$value['cexit']."','".$value['chexit']."','".$value['pitch']."','".$value['height']."','".$value['LineType']."','".$value['newlength']."','".$value['newdepth']."','".$value['edge']."','".$_POST['qty'][$key]."','".$_POST['price'][$key]."','0',now())";

					    $conn->query($sqlItem);
			    	}
			    	unset($_SESSION['client_mail']);
			    	unset($_SESSION['client_mail_send']);
					header('location:admin-email-template.php');
			    }
			} else {
			    echo "Error: " . $sql . "<br>" . $conn->error;
			}
			
			$conn->close();
		}
		// echo "<pre>"; print_r($_SESSION); exit();
	?>
  <!-- navbar-->
  <header class="header">
    <nav class="navbar navbar-expand-lg">
      <div class="container">
        <a href="#intro" class="navbar-brand link-scroll">
          <img src="http://pixalux.totalsimplicity.com.au/img/logo.png" alt="" class="img-fluid">
        </a>
        <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
          aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right">
          <i class="fa fa-bars"></i>
        </button>
        <div id="navbarSupportedContent" class="collapse navbar-collapse">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a href="#intro" class="nav-link link-scroll">Home</a>
            </li>
            <li class="nav-item">
              <a href="#about" class="nav-link link-scroll">About </a>
            </li>
            <li class="nav-item">
              <a href="#services" class="nav-link link-scroll">Services</a>
            </li>
            <li class="nav-item">
              <a href="#portfolio" class="nav-link link-scroll">Portfolio</a>
            </li>
            <li class="nav-item">
              <a href="#text" class="nav-link link-scroll">Text</a>
            </li>
            <li class="nav-item">
              <a href="#contact" class="nav-link link-scroll">Contact</a>
            </li>
            <li class="nav-item">
              <a href="cart.php" class="nav-link link-scroll"><img src="cart.png" height="30" width="30"></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  
   <!-- checkout-->
  <section id="checkout" class="text clearfix">
	<div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h1>Checkout</h1>
        </div>
      </div>
	  
	  <div class="row">
	  <div class="col-md-8 left">
		
		
	  
		<section class="checkout-step -unreachable" id="checkout-addresses-step">
		<h1 class="step-title">Addresses</h1>
			
			<div class="js-address-form">
				<form method="POST" action="thankyou.php">
				<p>The selected address will be used both as your personal address (for invoice) and as your delivery address.</p>
				
				<div id="delivery-address">
					<div class="js-address-form">
						<div class="form-fields">
							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">First name</label>
								<div class="col-md-6"><input class="form-control" name="firstname" value="" maxlength="32" required="" type="text"></div>
								<div class="col-md-3 form-control-comment"></div>
							</div>
							
							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">Last name</label>
								<div class="col-md-6"><input class="form-control" name="lastname" value="" maxlength="32" required="" type="text"></div>
								<div class="col-md-3 form-control-comment"></div>
							</div>

							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">Email</label>
								<div class="col-md-6"><input class="form-control" name="email" value="" maxlength="32" required="" type="email"></div>
								<div class="col-md-3 form-control-comment"></div>
							</div>

							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">Address</label>
								<div class="col-md-6"><input class="form-control" name="address1" value="" maxlength="128" required="" type="text"></div>
								<div class="col-md-3 form-control-comment"></div>
							</div>
							
							<div class="form-group row ">
								<label class="col-md-3 form-control-label">Address Complement</label>
								<div class="col-md-6"><input class="form-control" name="address2" value="" maxlength="128" type="text"></div>
								<div class="col-md-3 form-control-comment">Optional</div>
							</div>
							
							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">City</label>
								<div class="col-md-6"><input class="form-control" name="city" value="" maxlength="64" required="" type="text"></div>
								<div class="col-md-3 form-control-comment"></div>
							</div>

							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">State</label>
								<div class="col-md-6">
									<select class="form-control form-control-select" name="state_id" required="">
									<option value="" disabled="" selected="">-- please choose --</option>
									  <option value="4">Alabama</option>
									  <option value="5">Alaska</option>
									  <option value="6">Arizona</option>
									  <option value="7">Arkansas</option>
									  <option value="8">California</option>
									  <option value="9">Colorado</option>
									  <option value="10">Connecticut</option>
									  <option value="11">Delaware</option>
									  <option value="12">Florida</option>
									  <option value="13">Georgia</option>
									  <option value="14">Hawaii</option>
									  <option value="15">Idaho</option>
									  <option value="16">Illinois</option>
									  <option value="17">Indiana</option>
									  <option value="18">Iowa</option>
									  <option value="19">Kansas</option>
									  <option value="20">Kentucky</option>
									  <option value="21">Louisiana</option>
									  <option value="22">Maine</option>
									  <option value="23">Maryland</option>
									  <option value="24">Massachusetts</option>
									  <option value="25">Michigan</option>
									  <option value="26">Minnesota</option>
									  <option value="27">Mississippi</option>
									  <option value="28">Missouri</option>
									  <option value="29">Montana</option>
									  <option value="30">Nebraska</option>
									  <option value="31">Nevada</option>
									  <option value="32">New Hampshire</option>
									  <option value="33">New Jersey</option>
									  <option value="34">New Mexico</option>
									  <option value="35">New York</option>
									  <option value="36">North Carolina</option>
									  <option value="37">North Dakota</option>
									  <option value="38">Ohio</option>
									  <option value="39">Oklahoma</option>
									  <option value="40">Oregon</option>
									  <option value="41">Pennsylvania</option>
									  <option value="42">Rhode Island</option>
									  <option value="43">South Carolina</option>
									  <option value="44">South Dakota</option>
									  <option value="45">Tennessee</option>
									  <option value="46">Texas</option>
									  <option value="47">Utah</option>
									  <option value="48">Vermont</option>
									  <option value="49">Virginia</option>
									  <option value="50">Washington</option>
									  <option value="51">West Virginia</option>
									  <option value="52">Wisconsin</option>
									  <option value="53">Wyoming</option>
									  <option value="54">Puerto Rico</option>
									  <option value="55">US Virgin Islands</option>
									  <option value="56">District of Columbia</option>
								  </select>
								</div>
								<div class="col-md-3 form-control-comment"></div>
						</div>
						
						<div class="form-group row ">
							<label class="col-md-3 form-control-label required">Zip/Postal Code</label>
							<div class="col-md-6"><input class="form-control" name="postcode" value="" maxlength="12" required="" type="text"></div>
							<div class="col-md-3 form-control-comment"></div>
						</div>
						
						<div class="form-group row ">
							<label class="col-md-3 form-control-label required">Country</label>
							<div class="col-md-6">
								<select class="form-control form-control-select js-country" name="country_id" required="">
								<option value="" disabled="" selected="">-- please choose --</option>
									  <option value="8">France</option>
									  <option value="1">Germany</option>
									  <option value="10">Italy</option>
									  <option value="15">Portugal</option>
									  <option value="21">United States</option>
								  </select>
							</div>
							<div class="col-md-3 form-control-comment"></div>
						</div>
						
						<div class="form-group row ">
							<label class="col-md-3 form-control-label">Phone</label>
							<div class="col-md-6"><input class="form-control" name="phone" value="" maxlength="32" type="text"></div>
							<div class="col-md-3 form-control-comment">Optional</div>
						</div>
						
						<div class="form-group row">
							<div class="col-md-9 col-md-offset-3">
							<input name="use_for_invoice" value="1" checked="" type="checkbox">
							<label>Use this address for invoice too</label>
							</div>
						</div>
						
				</div>
				
				<footer class="form-footer clearfix">
					<button type="submit" class="continue btn btn-primary button-small pull-xs-right button-small" name="confirm-addresses" value="1">
					  Continue
				  </button>
				  <a class="js-cancel-address cancel-address btn btn-primary button-small pull-xs-right button-small" href="?cancelAddress=delivery">Cancel</a>
				</footer>
				
				
			</div>
			</div>
			
			</form>
				  
				  
				  
				
			  </div>
		
		</section>
		
	  
	  </div><!-- end of col -->
	  <div class="col-md-4 right">
		
		<div class="right-box">
					<section id="js-checkout-summary" class="card js-cart" data-refresh-url="#">
					  <div class="card-block">
						<div class="cart-summary-products">
							<p>1 item</p>
							<p><a href="#" data-toggle="collapse" data-target="#cart-summary-product-list">show details</a></p>
							<div class="collapse" id="cart-summary-product-list">
							<ul class="media-list clearfix">
							<li class="media  clearfix">
								<div class="media-left">
								<a href="#" title="Product Name"><img class="media-object" src="product-img.png" class="img-fluid" alt="Product Name"></a>
								</div>
								<div class="media-body">
									<span class="product-name">Product Name</span>
									<span class="product-quantity">x2</span>
									<span class="product-price float-xs-right">$28.72</span>
								</div>
							</li>
							</ul>
							</div>
						</div>
						
						<div class="cart-summary-line cart-summary-subtotals" id="cart-subtotal-products">
							<span class="label">Subtotal</span>
							<span class="value pull-right">$<?php echo isset($_SESSION['cart']['item_price'])?$_SESSION['cart']['item_price']:'0'; ?></span>
						</div>
						
						<div class="cart-summary-line cart-summary-subtotals" id="cart-subtotal-shipping">
							<span class="label">Shipping</span>
							<span class="value pull-right">$<?php echo isset($_SESSION['cart']['shipping'])?$_SESSION['cart']['shipping']:0; ?></span>
						</div>
						
					</div>
					
					<div class="block-promo">
						<div class="cart-voucher">
						<p>
						<a class="collapse-button promo-code-button" data-toggle="collapse" href="#promo-code" aria-expanded="false" aria-controls="promo-code">
						Have a promo code?
						</a>
						</p>
						
						<div class="promo-code collapse" id="promo-code">
							<form action="#" data-link-action="add-voucher" method="post">
								<input class="form-control promo-input" name="discount_name" placeholder="Promo code" type="text">
								<button type="submit" class="btn btn-primary"><span>Add</span></button>
							</form>
							
						</div>
					</div>
				</div>
				
				<hr>
				
				<div class="card-block cart-summary-totals">
					<div class="cart-summary-line cart-total">
						<span class="label">Total (tax excl.)</span>
						<span class="value pull-right">$<?php echo isset($_SESSION['cart']['total_price'])?$_SESSION['cart']['total_price']:0; ?></span>
					</div>
					
					<div class="cart-summary-line">
						<span class="label sub">Taxes</span>
						<span class="value sub pull-right">$0.00</span>
					</div>
				</div>
				
				<hr>
				
				
				<div id="block-reassurance">
				<ul>
						  <li>
					  <div class="block-reassurance-item">
						<span class="reassurance-img">
							<img src="ic_verified_user_black_36dp_1x.png" alt="Security policy (edit with Customer reassurance module)">
						</span>
						<span>Security policy</span>
					  </div>
					</li>
						  <li>
					  <div class="block-reassurance-item">
						<span class="reassurance-img">
							<img src="ic_local_shipping_black_36dp_1x.png" alt="Delivery policy (edit with Customer reassurance module)">
						</span>
						<span>Delivery policy</span>
					  </div>
					</li>
						  <li>
					  <div class="block-reassurance-item">
						<span class="reassurance-img">
							<img src="ic_swap_horiz_black_36dp_1x.png" alt="Return policy (edit with Customer reassurance module)">
						</span>
						<span>Return policy</span>
					  </div>
					</li>
					  </ul>
			  </div><!-- end of block-reassurance -->
				
				</section>
				
				
				
		</div>
		
	  </div><!-- end of col -->
	  
	  </div><!-- end of row -->
	 

	 </div><!-- end of container -->
  
  </section>
  
  <footer style="background-color: #98999A;">
    <div class="container">
      <div class="row copyright">
        <div class="col-md-6">
          <p class="mb-md-0 text-center text-md-left">&copy;2018 PIXALUX MANUFACTURING AUSTRLIA</p>
        </div>
        <div class="col-md-6">
          <p class="credit mb-md-0 text-center text-md-right">Created by
            <a href="https://www.Leapfrogmarket.com.au">Leapfrog Market</a>
          </p>
        </div>
      </div>
    </div>
  </footer>
  <!-- JavaScript files-->
  <script src="http://pixalux.totalsimplicity.com.au/vendor/jquery/jquery.min.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/popper.js/umd/popper.min.js">
  </script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/jquery.cookie/jquery.cookie.js">
  </script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/jquery.cookie/jquery.cookie.js">
  </script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/lightbox2/js/lightbox.min.js"></script>
  </script>
  <script src="front.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/js/svg.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/app/app.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/app/designs/read-designs.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/app/products/create-product.js"></script>
  
  
  <link href="jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" media="all">
   <script src="jquery.bootstrap-touchspin.js"></script>
  
   <script>
            $("input[name='qty']").TouchSpin({
                min: 0,
                max: 1000,
                stepinterval: 50,
                maxboostedstep: 100
            });
        </script>
  
</body>

</html>