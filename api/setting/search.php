<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/setting.php';
 
// instantiate database and setting object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$setting = new setting($db);
 
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
 
// query settings
$stmt = $setting->search($keywords);
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // settings array
    $settings_arr=array();
    $settings_arr["records"]=array();

    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $setting_item=array(
            "id" => $id,
            "name" => $name,
            "value" => json_decode($value),
            "type" => $type
        );
 
        array_push($settings_arr["records"], $setting_item);
    }
 
    echo json_encode($settings_arr);
}
 
else{
    echo json_encode(
        array("message" => "No settings found.")
    );
}
?>
