<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/setting.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare setting object
$setting = new setting($db);
 
// set ID property of setting to be edited
$setting->id = isset($_GET['id']) ? $_GET['id'] : die();
 
// read the details of setting to be edited
$setting->readOne();
 
// create array
$arr = array(
    "id" =>  $setting->id,
    "name" => $setting->name,
    "value" => json_decode($setting->value),
    "type" => $setting->type);
 
// make it json format
print_r(json_encode($arr));
?>
