<?php
class drawing{
 
    // database connection and table name
    private $conn;
    private $table_name = "drawings";
 
    // object properties
    public $id;
    public $name;
    public $value;
    public $content;
    public $type;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read drawings
    function read(){
    
        // select all query
        $query = "SELECT
                    d.id, d.name, d.value, d.content, d.type
                FROM
                    " . $this->table_name . " d
                ORDER BY
                    d.name DESC";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // used when filling up the update drawing form
    function readOne($status = false){
        
        // query to read single record
        $query = "SELECT
                    d.id, d.name, d.value, d.content, d.type
                FROM
                    " . $this->table_name . " d
                WHERE
                    d.id = ?
                LIMIT
                    0,1";
    
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
    
        // bind id of drawing to be updated
        $stmt->bindParam(1, $this->id);
    
        // execute query
        $stmt->execute();
    
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if($status){
            return $row;
        }
        // set values to object properties
        $this->id = $row['id'];
        $this->name = $row['name'];
        $this->value = $row['value'];
        $this->content = $row['content'];
        $this->type = $row['type'];
    }

    // used when filling up the update drawing form
    function readType($keywords){
  
        // select all query
        $query = "SELECT
                    d.id, d.name, d.content, d.type
                FROM
                    " . $this->table_name . " d
                WHERE
                    d.type = ?
                ORDER BY
                    d.name";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $keywords=htmlspecialchars(strip_tags($keywords));
        //$keywords = "%{$keywords}%";
    
        // bind
        $stmt->bindParam(1, $keywords);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // create drawing
    function create(){
    
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                name=:name, value=:value, content=:content, type=:type";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->value=htmlspecialchars(strip_tags($this->value));
        $this->content=htmlspecialchars(strip_tags($this->content));
        $this->type=htmlspecialchars(strip_tags($this->type));
    
        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":content", $this->value);
        $stmt->bindParam(":type", $this->content);
        $stmt->bindParam(":type", $this->type);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
        
    }

    // update the drawing
    function update(){
    
        // update query
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    name = :name,
                    value = :value,
                    content = :content,
                    type = :type
                WHERE
                    id = :id";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->value=htmlspecialchars(strip_tags($this->value));
        $this->content=htmlspecialchars(strip_tags($this->content));
        $this->type=htmlspecialchars(strip_tags($this->type));
    
        // bind new values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(':value', $this->value);
        $stmt->bindParam(":content", $this->content);
        $stmt->bindParam(":type", $this->type);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    // delete the drawing
    function delete(){
    
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
    
        // bind id of record to delete
        $stmt->bindParam(1, $this->id);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
        
    }

    // search drawings
    function search($keywords){
    
        // select all query
        $query = "SELECT
                    d.id, d.name, d.value, d.content, d.type
                FROM
                    " . $this->table_name . " d
                WHERE
                    d.name LIKE ? OR d.value LIKE ? OR d.content LIKE ? OR d.type LIKE ?
                ORDER BY
                    d.name DESC";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $keywords=htmlspecialchars(strip_tags($keywords));
        $keywords = "%{$keywords}%";
    
        // bind
        $stmt->bindParam(1, $keywords);
        $stmt->bindParam(2, $keywords);
        $stmt->bindParam(3, $keywords);
        $stmt->bindParam(4, $keywords);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // read drawings with pagination
    public function readPaging($from_record_num, $records_per_page){
    
        // select query
        $query = "SELECT
                    d.id, d.name, d.value, d.content, d.type
                FROM
                    " . $this->table_name . " d
                ORDER BY d.name DESC
                LIMIT ?, ?";
    
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
    
        // bind variable values
        $stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
        $stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);
    
        // execute query
        $stmt->execute();
    
        // return values from database
        return $stmt;
    }

    // used for paging drawings
    public function count(){
        $query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
    
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
        return $row['total_rows'];
    }
}
?>
