<?php
session_start();    

class Product{
 
    // database connection and table name
    private $conn;
    private $table_name = "products";
 
    // object properties
    public $id;
    public $session_id;
    public $lenght;
    public $depth;
    public $edge_color;
    public $Line_type;
    public $led_color;
    public $number_edge;
    public $exit_topbottom;
    public $exit_leftrtight;
    public $border_height;
    public $led_pitch;
    public $led_id;
    public $botside_id;
    public $botprofile_id;
    public $topside_id;
    public $topprofile_id;
    public $cable_id;
    public $date_created;

 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read products
    function read(){
    
        // select all query
        $query = "SELECT *
                FROM
                    " . $this->table_name . " p
                ORDER BY
                    p.date_created DESC";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // used when filling up the update product form
    function readOneSession(){
    
        // query to read single record
        $query = "SELECT *
                FROM
                    " . $this->table_name . " p
                WHERE
                    p.session_id = ?";
    
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
    
        // bind id of product to be updated
        $stmt->bindParam(1, $this->session_id);
    
        // execute query
        $stmt->execute();
    
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
        // set values to object properties
        $this->id = $row['id'];
        $this->session_id = $row['session_id'];
        $this->lenght = $row['lenght'];
        $this->depth = $row['depth'];
        $this->edge_color = $row['edge_color'];
        $this->Line_type = $row['Line_type'];
        $this->led_color = $row['led_color'];
        $this->number_edge = $row['number_edge'];
        $this->exit_topbottom = $row['exit_topbottom'];
        $this->exit_leftrtight = $row['exit_leftrtight'];
        $this->border_height = $row['border_height'];
        $this->led_pitch = $row['led_pitch'];
        $this->led_id = $row['led_id'];
        $this->botside_id = $row['botside_id'];
        $this->botprofile_id = $row['botprofile_id'];
        $this->topside_id = $row['topside_id'];
        $this->topprofile_id = $row['topprofile_id'];
        $this->cable_id = $row['cable_id'];
        $this->date_created = $row['date_created'];

    }

    // create product
    function create(){
    
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    session_id=:session_id,	
                    lenght=:lenght,
                    depth=:depth,
                    edge_color=:edge_color,
                    Line_type=:Line_type,
                    led_color=:led_color,
                    number_edge=:number_edge,
                    exit_topbottom=:exit_topbottom,
                    exit_leftrtight=:exit_leftrtight,
                    border_height=:border_height,
                    led_pitch=:led_pitch,
                    led_id=:led_id,
                    botside_id=:botside_id,
                    botprofile_id=:botprofile_id,
                    topside_id=:topside_id,
                    topprofile_id=:topprofile_id,
                    cable_id=:cable_id";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // bind values
        $stmt->bindParam(":session_id", $this->session_id);
        $stmt->bindParam(":lenght", $this->lenght);
        $stmt->bindParam(":depth", $this->depth);
        $stmt->bindParam(":edge_color", $this->edge_color);
        $stmt->bindParam(":Line_type", $this->Line_type);
        $stmt->bindParam(":led_color", $this->led_color);
        $stmt->bindParam(":number_edge", $this->number_edge);
        $stmt->bindParam(":exit_topbottom", $this->exit_topbottom);
        $stmt->bindParam(":exit_leftrtight", $this->exit_leftrtight);
        $stmt->bindParam(":border_height", $this->border_height);
        $stmt->bindParam(":led_pitch", $this->led_pitch);
        $stmt->bindParam(":led_id", $this->led_id);
        $stmt->bindParam(":botside_id", $this->botside_id);
        $stmt->bindParam(":botprofile_id", $this->botprofile_id);
        $stmt->bindParam(":topside_id", $this->topside_id);
        $stmt->bindParam(":topprofile_id", $this->topprofile_id);
        $stmt->bindParam(":cable_id", $this->cable_id);

    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
        
    }

    // update the product
    function update(){
    
        // update query
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    session_id=:session_id,	
                    lenght=:lenght,
                    depth=:depth,
                    edge_color=:edge_color,
                    Line_type=:Line_type,
                    led_color=:led_color,
                    number_edge=:number_edge,
                    exit_topbottom=:exit_topbottom,
                    exit_leftrtight=:exit_leftrtight,
                    border_height=:border_height,
                    led_pitch=:led_pitch,
                    led_id=:led_id,
                    botside_id=:botside_id,
                    botprofile_id=:botprofile_id,
                    topside_id=:topside_id,
                    topprofile_id=:topprofile_id,
                    cable_id=:cable_id
            WHERE
                    id = :id";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // bind new values
        $stmt->bindParam(":session_id", $this->session_id);
        $stmt->bindParam(":lenght", $this->lenght);
        $stmt->bindParam(":depth", $this->depth);
        $stmt->bindParam(":edge_color", $this->edge_color);
        $stmt->bindParam(":Line_type", $this->Line_type);
        $stmt->bindParam(":led_color", $this->led_color);
        $stmt->bindParam(":number_edge", $this->number_edge);
        $stmt->bindParam(":exit_topbottom", $this->exit_topbottom);
        $stmt->bindParam(":exit_leftrtight", $this->exit_leftrtight);
        $stmt->bindParam(":border_height", $this->border_height);
        $stmt->bindParam(":led_pitch", $this->led_pitch);
        $stmt->bindParam(":led_id", $this->led_id);
        $stmt->bindParam(":botside_id", $this->botside_id);
        $stmt->bindParam(":botprofile_id", $this->botprofile_id);
        $stmt->bindParam(":topside_id", $this->topside_id);
        $stmt->bindParam(":topprofile_id", $this->topprofile_id);
        $stmt->bindParam(":cable_id", $this->cable_id);
        $stmt->bindParam(':id', $this->id);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    // delete the product
    function delete(){
    
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
    
        // bind id of record to delete
        $stmt->bindParam(1, $this->id);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
        
    }

}
?>
