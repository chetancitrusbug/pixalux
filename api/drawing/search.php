<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/drawing.php';
 
// instantiate database and drawing object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$drawing = new drawing($db);
 
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
 
// query drawings
$stmt = $drawing->search($keywords);
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // drawings array
    $drawings_arr=array();
    $drawings_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $drawing_item=array(
            "id" => $id,
            "name" => $name,
            "value" => json_decode($value),
            "content" => $content,
            "type" => $type
        );
 
        array_push($drawings_arr["records"], $drawing_item);
    }
 
    echo json_encode($drawings_arr);
}
 
else{
    echo json_encode(
        array("message" => "No drawings found.")
    );
}
?>
