<?php session_start(); 

    include_once 'api/config/database.php';
    
    if(!empty($_POST)){
        $db = new Database();

        // Create connection
        $conn = new mysqli($db->host, $db->username, $db->password, $db->db_name);
        $_POST['use_for_invoice'] = isset($_POST['use_for_invoice'])?$_POST['use_for_invoice']:'';


        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 
        $_SESSION['client_email_id'] = $_POST['email'];
        $sql = "INSERT INTO
                    customer_detail (first_name,last_name,email,address1,address2,city,state_id,postcode,country_id,phone,use_for_invoice,deleted,created_at)
                VALUES
                    ('".$_POST['firstname']."','".$_POST['lastname']."','".$_POST['email']."','".$_POST['address1']."','".$_POST['address2']."','".$_POST['city']."','".$_POST['state_id']."','".$_POST['postcode']."','".$_POST['country_id']."','".$_POST['phone']."','".$_POST['use_for_invoice']."','0',now())";

        if ($conn->query($sql) === TRUE) {
            $_SESSION['client_mail'] = 1;
            // echo "<pre>"; print_r($_SESSION); exit();
            header('location:admin-email-template.php');
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
        

        $conn->close();
    }
    
    // echo "<pre>"; print_r('here'); exit();
    if(isset($_SESSION['client_mail_send'])){
        if($_GET['load'] != 1){
            header('Location:thankyou.php?load=1');
        }
        session_destroy();
    }
        
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Thank You : Black And White Bootstrap Landing Page / Portfolio</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="http://pixalux.totalsimplicity.com.au/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="http://pixalux.totalsimplicity.com.au/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Lightbox-->
    <link rel="stylesheet" href="http://pixalux.totalsimplicity.com.au/vendor/lightbox2/css/lightbox.min.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="http://pixalux.totalsimplicity.com.au/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="http://pixalux.totalsimplicity.com.au/img/favicon.ico">
    <!-- Tweaks for older IEs-->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>

<body>
  <!-- navbar-->
        <header class="header">
            <nav class="navbar navbar-expand-lg">
              <div class="container">
                <a href="#intro" class="navbar-brand link-scroll">
                  <img src="http://pixalux.totalsimplicity.com.au/img/logo.png" alt="" class="img-fluid">
                </a>
                <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                  aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right">
                  <i class="fa fa-bars"></i>
                </button>
                <div id="navbarSupportedContent" class="collapse navbar-collapse">
                  <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                      <a href="#intro" class="nav-link link-scroll">Home</a>
                    </li>
                    <li class="nav-item">
                      <a href="#about" class="nav-link link-scroll">About </a>
                    </li>
                    <li class="nav-item">
                      <a href="#services" class="nav-link link-scroll">Services</a>
                    </li>
                    <li class="nav-item">
                      <a href="#portfolio" class="nav-link link-scroll">Portfolio</a>
                    </li>
                    <li class="nav-item">
                      <a href="#text" class="nav-link link-scroll">Text</a>
                    </li>
                    <li class="nav-item">
                      <a href="#contact" class="nav-link link-scroll">Contact</a>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
        </header>
  
        <!-- cart-->
        <section id="cart" class="text">
            <div class="container">
                <div class="row">
                	<div class="success-div-2">
                		<h1>Thank You!</h1>
                		<span class="span-icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                		<h2>Payment Complete</h2>
                		<p>You have been successfully completed order. We have emailed you your order.</p>
                		<div class="text-center"><a href="http://pixalux.totalsimplicity.com.au/" class="a-success">Home</a></div>
            	   </div>
    	        </div><!-- end of row -->
            </div><!-- end of container -->
	   </section><!-- end of cart -->
  
        <footer style="background-color: #98999A;">
            <div class="container">
                <div class="row copyright">
                    <div class="col-md-6">
                      <p class="mb-md-0 text-center text-md-left">&copy;2018 PIXALUX MANUFACTURING AUSTRLIA</p>
                    </div>
                    <div class="col-md-6">
                        <p class="credit mb-md-0 text-center text-md-right">Created by
                            <a href="https://www.Leapfrogmarket.com.au">Leapfrog Market</a>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- JavaScript files-->
        <script src="http://pixalux.totalsimplicity.com.au/vendor/jquery/jquery.min.js"></script>
        <script src="http://pixalux.totalsimplicity.com.au/vendor/popper.js/umd/popper.min.js">
        </script>
        <script src="http://pixalux.totalsimplicity.com.au/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="http://pixalux.totalsimplicity.com.au/vendor/jquery.cookie/jquery.cookie.js">
        </script>
        <script src="http://pixalux.totalsimplicity.com.au/vendor/jquery.cookie/jquery.cookie.js">
        </script>
        <script src="http://pixalux.totalsimplicity.com.au/vendor/lightbox2/js/lightbox.min.js"></script>
        </script>
        <script src="front.js"></script>
        <script src="http://pixalux.totalsimplicity.com.au/js/svg.js"></script>
        <script src="http://pixalux.totalsimplicity.com.au/app/app.js"></script>
        <script src="http://pixalux.totalsimplicity.com.au/app/designs/read-designs.js"></script>
        <script src="http://pixalux.totalsimplicity.com.au/app/products/create-product.js"></script>
  
  
        <link href="jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" media="all">
        <script src="jquery.bootstrap-touchspin.js"></script>
        <script>
            $("input[name='qty']").TouchSpin({
                min: 0,
                max: 1000,
                stepinterval: 50,
                maxboostedstep: 100
            });
        </script>
    </body>

</html>