jQuery(document).ready(function($) {
	$('.qty').on('change',function(){
    	var price = 28.72;
        var key = $(this).data('key');
        var qty = $(this).val();
        price = (qty*price);
		$('.price-'+key).val(price.toFixed(2));
        $('.price-'+key).html(price.toFixed(2));
        $('.qty-'+key).val(qty);
        calculateTotal();
    });
    calculateTotal();
    function calculateTotal(){
    	$totalQty = $totalPrice = 0;
    	$shippingCharge = parseFloat($('.shipping_charge').html());
	    $.each($('.price_loop :input'),function(index, el) {
	    	if(el.getAttribute('data-tag') == 'qty'){
	    		$totalQty = parseInt($totalQty) + parseInt(el.value);
	    	}else{
	    		$totalPrice = parseFloat($totalPrice) + parseFloat(el.value);
	    	}
	    });
	    $('.total_qty_disp').html($totalQty);
	    $('.total_qty_disp').val($totalQty);
	    $('.total_price_disp').html($totalPrice);
	    $('.total_price_disp').val($totalPrice);
	    $('.total_price_text').val($totalPrice + $shippingCharge);
	    $('.total_price_text').html($totalPrice + $shippingCharge);
    }
});