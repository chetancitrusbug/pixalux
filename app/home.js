var draw = SVG('pixalux').size(DrawingSize, DrawingSize);

//global variables
var global_olength = parseInt($("#length").val());
var global_odepth = parseInt($("#depth").val());
var global_ncolor = $("#mycp2").val();
var global_ltype;
var global_ledcolor;
var global_nedge;
var global_cexit;
var global_chexit;
var global_bheight;
var global_pitch;

var global_LedID;
var global_BotsideID;
var global_BotprofileID;

var global_TopsideID;
var global_TopprofileID;
var global_CableID;
var global_TopCableID;

//initial settings
profileDrawing();
Dropdown('led', 'Led');
Dropdown('cexit', 'Arrow');
Dropdown('side', 'Lit');
Draw('top', false, 5);
Draw('bot', false, 5);
if($('input[name="key"]').val() == ''){
    draw_pixalux(global_olength, global_odepth, global_ncolor, '#DAE1E5', 16, global_cexit, global_chexit);
}

$(document).ready(function () {
    $("#success-alert").hide();
    
    $("#led").on("change paste keyup", function () {
        $('input[name="led_name"]').val($(this).find('option:selected').text());
        global_odepth = $("#depth").val();
        global_olength = $("#length").val();
        global_ncolor = $('#mycp2').val();
        global_cexit = $('#cexit').find('option:selected').text();
        global_ledcolor = $("#colour").val();
        global_bheight = $("#height").val();
        global_LedID = $("#led").val();
        global_nedge = $("#edge").val();
        global_ltype = $("#LineType").val();
        global_pitch = $("#pitch").val();
        findPitch(global_LedID);
        findColour(global_LedID, "colour");
        Draw('top', global_LedID);
        Draw('bot', global_LedID);
        imageUpdate();
    });

    $("#cexit").on("change paste keyup", function () {
        $('input[name="cable_side"]').val($(this).find('option:selected').text());
        global_odepth = $("#depth").val();
        global_olength = $("#length").val();
        global_cexit = $('#cexit').find('option:selected').text();
        console.log(global_cexit);
        global_ledcolor = $("#colour").val();
        global_bheight = $("#height").val();
        global_CableID = $("#cexit").val();
        global_nedge = $("#edge").val();
        global_ltype = $("#LineType").val();
        Draw('bot', false, false, false, global_CableID);
        global_TopCableID = BotTopNew(global_CableID);
        findMaxLength(global_odepth,global_BotprofileID);
        update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, global_bheight, global_ltype, global_nedge, global_cexit, global_chexit);
        imageUpdate();
    });

    $("#chexit").on("change paste keyup", function () {
        $('input[name="cable_exit"]').val($(this).find('option:selected').text());
        global_odepth = $("#depth").val();
        global_olength = $("#length").val();
        global_cexit = $('#cexit').find('option:selected').text();
        global_ledcolor = $("#colour").val();
        global_bheight = $("#height").val();
        global_CableID = $("#cexit").val();
        global_nedge = $("#edge").val();
        global_ltype = $("#LineType").val();
        global_chexit = $("#chexit").val();
        Draw('bot', false, false, false, global_CableID);
        update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, global_bheight, global_ltype, global_nedge, global_cexit, global_chexit);
        imageUpdate();
    });

    $("#side").on("change paste keyup", function () {
        $('input[name="lit_name"]').val($(this).find('option:selected').text());
        global_odepth = $("#depth").val();
        global_olength = $("#length").val();
        global_cexit = $('#cexit').find('option:selected').text();
        global_ledcolor = $("#colour").val();
        global_bheight = $("#height").val();
        global_BotsideID = $("#side").val();
        global_nedge = $("#edge").val();
        global_ltype = $("#LineType").val();
        global_TopsideID = BotTopNew(global_BotsideID);
        Draw('top', false, global_TopsideID);
        Draw('bot', false, global_BotsideID);
        update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, global_bheight, global_ltype, global_nedge, global_cexit, global_chexit);
        imageUpdate();
    });
  
    $('#button1,#button2,#button3,#button4,#button5,#button6,#button7,#button8').on("click", function () {
      //console.log(global_BotprofileID);
        $('input[name="profile_name"]').val($(this).val());
        global_odepth = $("#depth").val();
        global_olength = $("#length").val();
        global_ncolor = $('#mycp2').val();
        global_cexit = $('#cexit').find('option:selected').text();
        global_ledcolor = $("#colour").val();
        global_bheight = $("#height").val();
        global_nedge = $("#edge").val();
        global_BotprofileID = $(this).attr('value');
        global_TopprofileID = BotTopNew(global_BotprofileID);
        global_ltype = $("#LineType").val();
        Draw('bot', false, false, global_BotprofileID);
        findHeight(global_BotprofileID);
        findMaxLength(global_odepth,global_BotprofileID);
        $('.drawprofile-'+profile).removeClass('highlight');
        imageUpdate();
    });

    $('#mycp1, #mycp2, #mycp3').on("change click paste keyup", function () {
        $('input[name="frame_color"]').val($(this).val());
        global_odepth = $("#depth").val();
        global_olength = $("#length").val();
        global_ncolor = $(this).val();
        global_cexit = $('#cexit').find('option:selected').text();
        global_ledcolor = $("#colour").val();
        global_bheight = $("#height").val();
        global_nedge = $("#edge").val();
        global_ltype = $("#LineType").val();
        update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, global_bheight, global_ltype, global_nedge, global_cexit, global_chexit);
        imageUpdate();
    });

    $("#depth").on("change click", function () {
        global_odepth = $("#depth").val();
        global_olength = $("#length").val();
        global_ncolor = $('#mycp2').val();
        global_cexit = $('#cexit').find('option:selected').text();
        global_ledcolor = $("#colour").val();
        global_bheight = $("#height").val();
        global_nedge = $("#edge").val();
        global_ltype = $("#LineType").val();
        //BotTopNew(global_BotprofileID);
        findMaxLength(global_odepth,global_BotprofileID);
        imageUpdate();
    });

    $("#length").on("change click", function () {
        global_odepth = $("#depth").val();
        global_olength = $("#length").val();
        global_ncolor = $('#mycp2').val();
        global_cexit = $('#cexit').find('option:selected').text();
        global_ledcolor = $("#colour").val();
        global_bheight = $("#height").val();
        global_nedge = $("#edge").val();
        global_ltype = $("#LineType").val();
        global_pitch = $("#pitch").val();
        findMaxDepth(global_olength, global_pitch);
        imageUpdate();
    });

    $("#mycp1").on("click", function () {
        global_odepth = $("#depth").val();
        global_olength = $("#length").val();
        global_ncolor = $('#mycp1').val();
        global_cexit = $('#cexit').find('option:selected').text();
        global_ledcolor = $("#colour").val();
        global_bheight = $("#height").val();
        global_nedge = $("#edge").val();
        global_ltype = $("#LineType").val();
        update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, global_bheight, global_ltype, global_nedge, global_cexit, global_chexit);
        imageUpdate();
    });

    $("#mycp2").on("click", function () {
        global_odepth = $("#depth").val();
        global_olength = $("#length").val();
        global_ncolor = $('#mycp2').val();
        global_cexit = $('#cexit').find('option:selected').text();
        global_ledcolor = $("#colour").val();
        bheight = $("#height").val();
        global_nedge = $("#edge").val();
        global_ltype = $("#LineType").val();
        update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, bheight, global_ltype, global_nedge, global_cexit, global_chexit);
        imageUpdate();
    });

    $("#mycp3").on("click", function () {
        global_odepth = $("#depth").val();
        global_olength = $("#length").val();
        global_ncolor = $('#mycp3').val();
        global_cexit = $('#cexit').find('option:selected').text();
        global_ledcolor = $("#colour").val();
        bheight = $("#height").val();
        global_nedge = $("#edge").val();
        global_ltype = $("#LineType").val();
        update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, bheight, global_ltype, global_nedge, global_cexit, global_chexit);
        imageUpdate();
    });

    function imageUpdate(){
        if($('input[name="key"]').val() != ''){
            $("#SvgjsSvg1001:eq(0)").hide();
        }
        $('input[name="first_part"]').val(document.getElementById("first_part").outerHTML);
        $('input[name="second_part"]').val(document.getElementById("profileCutBottom").outerHTML);
        $('input[name="third_part"]').val(document.getElementById("pixalux").outerHTML);
    }

    if($('input[name="key"]').val() != ''){
        $('#led').val($('.led_id').data('id'));
        $('#side').val($('.side_id').data('id'));
        $('#cexit').val($('.cexit_id').data('id'));
        $('#chexit').val($('.chexit_id').data('id')); 

        $('#pixalux').attr({
            style: 'height:466px;',
        });
        
        var profile = $('input[name="profile_name"]').val();
        $('.drawprofile-'+profile).addClass('highlight');

        var color = $('input[name="frame_color"]').val();
        if(color == '#000'){
            $("#mycp1").attr('style','border:3px solid #6c757d;');
        }
        if(color == '#666'){
            $("#mycp2").attr('style','border:3px solid #6c757d;');
        }
        if(color == '#fff'){
            $("#mycp3").attr('style','border:3px solid #6c757d;');
        }

        /*var profile = $('input[name="profile_name"]').val();
        $('.drawprofile-'+profile).trigger('click');
        $('.drawprofile-'+profile).addClass('highlight');

        global_ncolor = $('input[name="frame_color"]').val();
        global_odepth = $("#depth").val();
        global_olength = $("#length").val();
        // global_ncolor = $('#mycp2').val();
        global_cexit = $('#cexit').find('option:selected').text();
        global_ledcolor = $("#colour").val();
        global_bheight = $("#height").val();
        global_nedge = $("#edge").val();
        global_BotprofileID = profile;
        global_TopprofileID = BotTopNew(global_BotprofileID);
        global_ltype = $("#LineType").val();
        Draw('bot', false, false, global_BotprofileID);
        findHeight(global_BotprofileID);
        findMaxLength(global_odepth,global_BotprofileID);

        $("#led").trigger('change');

        var color = $('input[name="frame_color"]').val();
        if(color == '#000'){
            $("#mycp1").trigger('click');
        }
        if(color == '#666'){
            $("#mycp2").trigger('click');
        }
        if(color == '#fff'){
            $("#mycp3").trigger('click');
        }*/
        /*$("#length").trigger('click');
        $("#cexit").trigger('change');
        $("#chexit").trigger('change');
        $("#side").trigger('change');*/
        // $("#length").trigger('change');
        // $("#length").focus();

    }

    /*$('.cart_button').on('click',function(e){
        e.preventDefault();

        var svgText = document.getElementById("first_part").outerHTML;
        var myCanvas = document.getElementById("canvas1");
        var ctxt = myCanvas.getContext("2d");
        drawInlineSVG(ctxt, svgText, function() {
            console.log(canvas1.toDataURL());  // -> PNG
            alert("see console for output...");
        });

        var svgText = document.getElementById("profileCutBottom").outerHTML;
        var myCanvas = document.getElementById("canvas2");
        var ctxt = myCanvas.getContext("2d");
        drawInlineSVG(ctxt, svgText, function() {
            console.log(canvas2.toDataURL());  // -> PNG
            alert("see console for output...");
        });

        var svgText = document.getElementById("SvgjsSvg1001").outerHTML;
        var myCanvas = document.getElementById("canvas3");
        var ctxt = myCanvas.getContext("2d");
        drawInlineSVG(ctxt, svgText, function() {
            console.log(canvas3.toDataURL());  // -> PNG
            alert("see console for output...");
        });

        $('input[name="first_part"]').val(document.getElementById("first_part").outerHTML);
        $('input[name="second_part"]').val(document.getElementById("profileCutBottom").outerHTML);
        $('input[name="third_part"]').val(document.getElementById("pixalux").outerHTML);
        $(this).submit();
        // debugger;
    });*/



});

function drawprofile(profileID) {
    console.log("drawprofile");
    global_odepth = $("#depth").val();
    global_olength = $("#length").val();
    global_ncolor = $('#mycp2').val();
    global_cexit = $('#cexit').find('option:selected').text();
    global_ledcolor = $("#colour").val();
    global_bheight = $("#height").val();
    global_nedge = $("#edge").val();
    global_BotprofileID = profileID;
    //BotTopNew(global_BotprofileID);
    global_ltype = $("#LineType").val();
    Draw('bot', false, false, global_BotprofileID);
    findHeight(global_BotprofileID);
    findMaxLength(global_odepth,global_BotprofileID);

};



function drawInlineSVG(ctx, rawSVG, callback) {

    var svg = new Blob([rawSVG], {type:"image/svg+xml;charset=utf-8"}),
        domURL = self.URL || self.webkitURL || self,
        url = domURL.createObjectURL(svg),
        img = new Image;
    
    img.onload = function () {
        ctx.drawImage(this, 0, 0);
        domURL.revokeObjectURL(url);
        callback(this);
    };
    
    img.src = url;
}


// usage:
