var draw = SVG('pixalux').size(DrawingSize, DrawingSize);

profileDrawing();
Dropdown('led', 'Led');
Dropdown('cexit', 'Arrow');
Dropdown('side', 'Lit');
Draw('top', false, 5);
Draw('bot', false, 5);

global_olength = $("#length").val();
global_odepth = $("#depth").val();
global_ncolor = $("#mycp2").val();
var global_cexit;
var global_chexit;
setTimeout(function(){
    draw_pixalux(global_olength, global_odepth, global_ncolor, '#DAE1E5', 16, global_cexit, global_chexit);
},500);

$(document).ready(function () {
    $("#length").on("change click", function () {
        pixalux('length');
    });

    $("#depth").on("change click", function () {
        pixalux('depth');
    });

    $("#led").on("change paste keyup", function () {
        $('input[name="led_name"]').val($(this).find('option:selected').text());
        pixalux('led');
    });

    $("#side").on("change paste keyup", function () {
        $('input[name="lit_name"]').val($(this).find('option:selected').text());
        pixalux('side');
    });

    $('#button1,#button2,#button3,#button4,#button5,#button6,#button7,#button8').on("click", function () {
        $('input[name="profile_name"]').val($(this).val());
        pixalux('button',$(this).val());
    });

    $('#mycp1, #mycp2, #mycp3').on("change click paste keyup", function () {
        $('input[name="frame_color"]').val($(this).val());
        pixalux('color',$(this).val());
    });

    $("#cexit").on("change paste keyup", function () {
        $('input[name="cable_side"]').val($(this).find('option:selected').text());
        pixalux('cexit');
    });

    $("#chexit").on("change paste keyup", function () {
        $('input[name="cable_exit"]').val($(this).find('option:selected').text());
        pixalux('chexit'); 
    });

    var profile;
    var color;
    if($('input[name="key"]').val() != ''){
        profile = $('input[name="profile_name"]').val();
        $('#led').val($('.led_id').data('id'));
        $('#side').val($('.side_id').data('id'));
        $('#cexit').val($('.cexit_id').data('id'));
        $('#chexit').val($('.chexit_id').data('id')); 

        $('.drawprofile-'+profile).addClass('highlight');

        color = $('input[name="frame_color"]').val();

        if(color == '#000'){
            $("#mycp1").addClass('border');
        }
        if(color == '#666'){
            $("#mycp2").addClass('border');
        }
        if(color == '#fff'){
            $("#mycp3").addClass('border');
        }

        setTimeout(function() {
            pixalux('length');
            pixalux('depth');
            pixalux('led');
            pixalux('side');
            pixalux('button',$('input[name="profile_name"]').val());
            pixalux('color',$('input[name="frame_color"]').val());
            pixalux('cexit');
            pixalux('chexit');
        }, 500);
        
    }

    function pixalux(key = '',value=''){
        global_olength = $("#length").val();
        global_odepth = $("#depth").val();
        global_LedID = $("#led").find('option:selected').val();
        global_BotsideID = $("#side").find('option:selected').val();
        global_cexit = $('#cexit').find('option:selected').text();
        global_CableID = $('#cexit').val();
        global_chexit = $("#chexit").val();
        var global_BotprofileID;

        global_ledcolor = $("#colour").val();
        global_bheight = $("#height").val();
        global_nedge = $("#edge").val();
        global_ltype = $("#LineType").val();
        global_pitch = $("#pitch").val();

        if(key == 'button'){
            global_BotprofileID = value;

            if(global_BotprofileID != profile){
                $('.drawprofile-'+profile).removeClass('highlight');
                profile = global_BotprofileID;
                $('.drawprofile-'+global_BotprofileID).addClass('highlight');
            }

            global_TopprofileID = BotTopNew(global_BotprofileID);
            Draw('bot', false, false, global_BotprofileID);
            findHeight(global_BotprofileID);
            findMaxLength(global_odepth,global_BotprofileID);
        }

        if(key == 'color'){
            global_ncolor = value;
            // alert(global_ncolor);
            if(global_ncolor != color){
                $("#mycp1").removeClass('border');
                $("#mycp2").removeClass('border');
                $("#mycp3").removeClass('border');
                color = value;
                if(global_ncolor == '#000'){
                    $("#mycp1").addClass('border');
                }
                if(global_ncolor == '#666'){
                    $("#mycp2").addClass('border');
                }
                if(global_ncolor == '#fff'){
                    $("#mycp3").addClass('border');
                }
            }
            update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, global_bheight, global_ltype, global_nedge, global_cexit, global_chexit);
        }else{
            if(color == 'undefined' && color == ""){
                global_ncolor = color;
            }else{
                global_ncolor = $("#mycp2").val();
            }
        }

        if(key == 'led'){
            global_ncolor = '#666';
            findPitch(global_LedID);
            findColour(global_LedID, "colour");
            Draw('top', global_LedID);
            Draw('bot', global_LedID);
        }

        if(key == 'cexit'){
            Draw('bot', false, false, false, global_CableID);
            global_TopCableID = BotTopNew(global_CableID);
            findMaxLength(global_odepth,global_BotprofileID);
            update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, global_bheight, global_ltype, global_nedge, global_cexit, global_chexit);
        }

        if(key == 'side'){
            global_TopsideID = BotTopNew(global_BotsideID);
            Draw('top', false, global_TopsideID);
            Draw('bot', false, global_BotsideID);
            update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, global_bheight, global_ltype, global_nedge, global_cexit, global_chexit);
        }

        if(key == 'chexit'){
            Draw('bot', false, false, false, global_CableID);
            update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, global_bheight, global_ltype, global_nedge, global_cexit, global_chexit);
        }

        if(key == 'length'){
            findMaxDepth(global_olength, global_pitch);
        }

        if(key == 'depth'){
            findMaxLength(global_odepth,global_BotprofileID);
        }
    }
});

function drawprofile(profileID) {
    $('input[name="profile_name"]').val(profileID);
    global_odepth = $("#depth").val();
    global_olength = $("#length").val();
    global_ncolor = $('#mycp2').val();
    global_cexit = $('#cexit').find('option:selected').text();
    global_ledcolor = $("#colour").val();
    global_bheight = $("#height").val();
    global_nedge = $("#edge").val();
    global_BotprofileID = profileID;
    //BotTopNew(global_BotprofileID);
    global_ltype = $("#LineType").val();
    Draw('bot', false, false, global_BotprofileID);
    findHeight(global_BotprofileID);
    findMaxLength(global_odepth,global_BotprofileID);
};

$(function() { 
    $(".cart_button").click(function(e) { 
        e.preventDefault();
        var svgText = document.getElementById("first_part").outerHTML;
        var myCanvas = document.getElementById("canvas1");
        var ctxt = myCanvas.getContext("2d");
        drawInlineSVG(ctxt, svgText, function() {
            console.log(canvas1[0].toDataURL());  // -> PNG
            $('input[name="first_part"]').val(canvas1[0].toDataURL());
            // alert("see console for output...");
        });

        var svgText = document.getElementById("profileCutBottom").outerHTML;
        var myCanvas = document.getElementById("canvas2");
        var ctxt = myCanvas.getContext("2d");
        drawInlineSVG(ctxt, svgText, function() {
            $('input[name="second_part"]').val(canvas2[0].toDataURL());
            console.log(canvas2[0].toDataURL());  // -> PNG
            // alert("see console for output...");
        });

        var svgText = document.getElementById("SvgjsSvg1001").outerHTML;
        var myCanvas = document.getElementById("canvas3");
        var ctxt = myCanvas.getContext("2d");
        drawInlineSVG(ctxt, svgText, function() {
            $('input[name="third_part"]').val(canvas3[0].toDataURL());
            console.log(canvas3[0].toDataURL());  // -> PNG
            // alert("see console for output...");
        });

        setTimeout(function() {
            $('#submit_form').submit();    
        }, 1000);
        
    });
}); 

function drawInlineSVG(ctx, rawSVG, callback) {

    var svg = new Blob([rawSVG], {type:"image/svg+xml;charset=utf-8"}),
        domURL = self.URL || self.webkitURL || self,
        url = domURL.createObjectURL(svg),
        img = new Image;
    
    img.onload = function () {
        ctx.drawImage(this, 150, 0, 150, 150);
        domURL.revokeObjectURL(url);
        callback(this);
    };
    
    img.src = url;
}