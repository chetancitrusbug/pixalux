<?php session_start();
$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
$message .= '<html xmlns="http://www.w3.org/1999/xhtml">';
$message .= '<head>';
$message .= '<meta name="description" content="Layout 1" />';
$message .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
$message .= '<meta name="viewport" content="width=device-width, initial-scale=1" />';
$message .= '<title>PIXALUX MANUFACTURING AUSTRLIA - Email Template</title>';
$message .= '<style>';
$message .= '@import url(http://fonts.googleapis.com/css?family=Open+Sans)';
$message .= '@media only screen and (max-width: 600px) {';
$message .= 'table[class=table600con] img {';
	$message .= 'max-width: 100% !important;';
	$message .= 'width: 100% !important;';
	$message .= 'margin: auto !important;';
	$message .= 'height: auto !important;';
$message .= '}';
$message .= 'div[class=wrapper] {';
	$message .= 'width: 90% !important;';
	$message .= 'background: #ffffff;';
$message .= '}';
$message .= 'div[class=wrapperout], td[class=grid_block] table {';
	$message .= 'width: 100% !important;';
$message .= '}';
$message .= 'table[class=table600none] {';
	$message .= 'height: 0 !important;';
	$message .= 'width: 0 !important;';
	$message .= 'display: none !important;';
$message .= '}';
$message .= 'table[class=table600full] {';
	$message .= 'width: 100% !important;';
$message .= '}';
$message .= 'table[class=table600con], table[class=table600] {';
	$message .= 'width: 90% !important;';
$message .= '}';
$message .= 'td[class=fw_full], td[class=fw_half], td[class=grid_block] {';
	$message .= 'width: 100% !important;';
	$message .= 'display: block !important;';
	$message .= 'text-align: center !important;';
$message .= '}';
$message .= 'td[class=spacer], table[class=line] {';
	$message .= 'height: 0 !important;';
	$message .= 'width: 0 !important;';
	$message .= 'display: none !important;';
$message .= '}';
$message .= 'td[class=grid_block] {';
	$message .= 'width: 100% !important;';
	$message .= 'text-align: center !important;';
	$message .= 'padding: 15px 0 !important;';
$message .= '}';
$message .= 'td[class=grid_block_nopad] {';
	$message .= 'width: 100% !important;';
	$message .= 'padding: 0 !important;';
	$message .= 'display: block !important;';
	$message .= 'text-align: center !important;';
$message .= '}';
$message .= 'td[class=grid_block] table {';
	$message .= 'margin: auto !important;';
$message .= '}';
$message .= 'tr[class=scaleimage] img {';
	$message .= 'width: auto !important';
$message .= '}';
$message .= 'td[class=boxpadding] {';
	$message .= 'width: 7% !important;';
$message .= '}';
$message .= 'td[class=boxcontent] {';
	$message .= 'width: 86% !important;';
$message .= '}';
$message .= 'div[class=divider] {';
	$message .= 'width: 70%;';
	$message .= 'height: 1px;';
	$message .= 'border-bottom: 1px solid #aaaaaa;';
	$message .= 'margin: 25px auto 0;';
$message .= '}';
$message .= 'td[class=grid_block] table[class=button_second], td[class=button] {';
	$message .= 'margin-top: 10px !important;';
$message .= '}';
$message .= 'table[class=buttonwrap], table[class=buttonwrap] tbody, table[class=buttonwrap] tr, td[class=button] {';
	$message .= 'width: 100% !important;';
	$message .= 'display: block;';
	$message .= 'height: auto;';
$message .= '}';
$message .= 'td[class=button] {';
	$message .= 'padding: 5px 0;';
$message .= '}';
$message .= '}';
$message .= 'a {';
	$message .= 'text-decoration: none;';
	$message .= 'color: #3a85d8;';
$message .= '}';
$message .= '.ReadMsgBody {';
	$message .= 'width: 100%;';
$message .= '}';
$message .= '.ExternalClass {';
	$message .= 'width: 100%;';
$message .= '}';
$message .= '.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {';
	$message .= 'line-height: 100%;';
$message .= '}';
$message .= '#outlook a {';
	$message .= 'padding: 0;';
$message .= '}';
$message .= 'span.yshortcuts {';
	$message .= 'color: #000;';
	$message .= 'background-color: none;';
	$message .= 'border: none;';
$message .= '}';
$message .= 'span.yshortcuts:hover, span.yshortcuts:active, span.yshortcuts:focus {';
	$message .= 'color: #000;';
	$message .= 'background-color: none;';
	$message .= 'border: none;';
$message .= '}';
$message .= 'body[class=body] {';
	$message .= 'min-width: 0 !important;';
$message .= '}';
$message .= '.third{
        overflow: hidden;
      }
      #SvgjsSvg1001{
        height: 100px;
        width: 100px;
      }
      .left{
        float: left;
      }
      .right{
        margin-top: 5px;
        float: right;
      }
      .second_image, .first_image{
      width: 100px;
      }
      .second_image{
        margin-top: 7px;
      }
      .third_image{
        position: absolute;
        clip: rect(19px,222px,111px,115px);
        width: 223px;
        border: 1px solid red;
        right: 15px;
        bottom: 39px;
      }';
$message .= '</style>';
$message .= '</head>';

$message .= '<body class="body" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;font-family: "Open Sans", sans-serif;font-size: 11pt;color: #777777; mso-line-height-rule: exactly;line-height: 1.5em;width: 100%; min-width: 600px;">';

  include_once 'api/config/database.php';
  include_once 'api/objects/drawing.php';
   
  // get database connection
  $database = new Database();
  $db = $database->getConnection();
   
  // prepare drawing object
  $drawing = new drawing($db);

$message .= '<table style="padding: 0;margin: 0;border-collapse: collapse;" class="bg" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#E3E3E3" align="center">';
  $message .= '<tbody>';
    $message .= '<tr>';
      $message .= '<td style="border-collapse: collapse;" width="100%" align="center"><repeater> </repeater>';
        $message .= '<repeater> </repeater>';
        $message .= '<repeater> </repeater>';
        $message .= '<repeater> </repeater>';
        $message .= '<repeater> </repeater>';
        $message .= '<repeater> </repeater>';
        $message .= '<repeater> </repeater>';
        $message .= '<repeater> </repeater>';
        $message .= '<repeater> </repeater>';
        $message .= '<repeater> </repeater>';
        $message .= '<repeater> </repeater>';
        $message .= '<table style="padding: 0;margin: 0;border-collapse: collapse; margin: auto;" class="table600full" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">';

          $message .= '<tbody>';
            $message .= '<tr bgcolor="#E3E3E3">';
              $message .= '<td class="full_width" style="border-collapse: collapse;" width="100%" align="center"><div class="wrapperout" style="width: 600px;margin: auto;">';
                  $message .= '<table class="table600con" style="border-collapse: collapse;" width="600" cellspacing="0" cellpadding="0" border="0">';
                    $message .= '<tbody>';
                      $message .= '<tr>';
                        $message .= '<td class="fw_full" style="padding-top: 15px;padding-bottom: 15px;font-size: 8pt;color: #666666;border-collapse: collapse;font-family: "Open Sans", sans-serif;;" mc:edit="online_link" width="600" align="center"><multiline label="Online Link Text"> Problem viewing? Click to <a href="#" style="color: #000000;text-decoration: none;outline: none;">view online</a> </multiline></td>';
                      $message .= '</tr>';
                    $message .= '</tbody>';
                  $message .= '</table>';
                $message .= '</div></td>';
            $message .= '</tr>';
            $message .= '<tr>
                          <td class="full_width" style="background: #ffffff; border-collapse: collapse; border-top: 3px solid #999a9b;" width="100%" align="center"><div class="wrapper" style="width: 600px;margin: auto;">
                              <table class="table600con" style="border-collapse: collapse;" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
                                <tbody>
                                  <tr class="scaleimage">
                                    <td class="spacer" style="border-collapse: collapse;" width="20"></td>
                                    <td class="grid_block" style="padding-top: 30px;padding-bottom: 30px;border-collapse: collapse;" width="270" valign="top" align="left"><img mc:edit="logo_img" editable="" src="http://pixalux.totalsimplicity.com.au/logo.png" alt="" style="max-width: 150px; display: block;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="150"></td>
                                    <td class="spacer" style="border-collapse: collapse;" width="20"></td>
                                    <td class="grid_block" style="line-height: 1.3em; padding-top: 30px;padding-bottom: 30px;color: #333333;border-collapse: collapse;font-family: "Open Sans", sans-serif; font-size: 9pt; font-weight: bold;" mc:edit="head_issue" width="270" valign="top" align="right"><multiline label="Header Call Us"> Order <span style="color: #222">#155</span>
                                        <p style="margin: 0;">August 08, 2018</p>
                                      </multiline></td>
                                    <td class="spacer" style="border-collapse: collapse;" width="20"></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div></td>
                        </tr>';
            $message .= '<tr mc:repeatable="block_3" >
                          <td class="full_width" style="border-collapse: collapse; padding-top: 0; padding-bottom: 20px; background:#f2f2f2;" width="100%" align="center"><div class="wrapper" style="width: 600px;margin: auto; background:#f2f2f2;">
                              <table class="table600con" style="border-collapse: collapse;" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#f2f2f2">
                                <tbody>
                                  <tr>
                                    <td class="spacer" style="border-collapse: collapse;" width="20"></td>
                                    <td class="grid_block" style="border-collapse: collapse; font-family: "Open Sans", sans-serif;; border-radius: 5px;" width="560" align="center">
                                    <table cellspacinxg="0" style="border-collapse: collapse;" width="100%" cellpadding="0" border="0">
                                        <tbody>
                                          <tr>
                                            <td class="boxpadding" style="border-collapse: collapse;" width="20"></td>
                                            <td class="boxcontent" style="border-collapse: collapse;font-family: "Open Sans", sans-serif;;" width="520" align="center">
                                            <div mc:edit="block_4">                                    
                                                <multiline label="Content">
                                                
                                                  <p style="margin: 0; margin-top: 1em; margin-bottom: 20px; font-size:30px; color:#000; font-weight:bold; text-transform:uppercase;">Purchased order #190030012</p>
                                                  <p style="margin: 0; margin-top: 1em; margin-bottom: 10px; font-size:14px; color:#000;font-weight:bold;">Placed on '. $_SESSION['created_at'] .' bd</p>
                                                  <p style="margin: 0; margin-top: 0; margin-bottom: 10px; font-size:12px; color:#666;">User Name | IP: 120.012.223.000 | usename@gmail.com | Mobile: +222-222-222-2222</p>
                                                </multiline>
                                              </div></td>
                                            <td class="boxpadding" style="border-collapse: collapse;" width="20"></td>
                                          </tr>
                                        </tbody>
                                      </table></td>
                                    <td class="spacer" style="border-collapse: collapse;" width="20"></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div></td>
                        </tr>';
            $message .= '<tr mc:repeatable="block_6">
                          <td class="full_width" style="border-collapse: collapse;" width="100%" align="center"><div class="wrapper" style="width: 600px;margin: auto;">
                              <table class="table600con" style="border-collapse: collapse;" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
                                <tbody>
                                  <tr>
                                    <td class="spacer" style="border-collapse: collapse;" width="20"></td>
                                    <td class="fw_full" style="border-collapse: collapse;" width="560" align="center"><table style="padding-top: 20px; padding-bottom: 20px;" width="100%" cellspacing="0" cellpadding="0">
                                        <tbody>
                                          
                                        </tbody>
                                      </table></td>
                                    <td class="spacer" style="border-collapse: collapse;" width="20"></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div></td>
                        </tr>';
            
              if(!empty($_SESSION[$_SESSION['ip']])){
                foreach ($_SESSION[$_SESSION['ip']] as $key => $value) {
                  $message .= '<tr mc:repeatable="block_7">
                                      <td class="full_width" style="border-collapse: collapse;" width="100%" align="center"><div class="wrapper" style="width: 600px;margin: auto;">
                                           <table class="table600con" style="border-collapse: collapse; border-top:1px solid #ddd;" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
                                            <tbody>
                                              <tr>
                                                <td class="spacer" style="border-collapse: collapse;" width="20"></td>
                                                <td class="grid_block" style="padding-top: 35px;padding-bottom: 25px;position: relative;border-collapse: collapse; font-family: "Open Sans", sans-serif;" width="270" valign="top" align="left">
                                                    <span class="product-image media-middle ">
                                                      <div class="left">
                                                        <img src="'.$value['first_part'].'" class="first_image"><br/>
                                                        <img src="'.$value['second_part'].'" class="second_image">
                                                      </div>
                                                      <div class="right">
                                                        <img src="'.$value['third_part'].'" class="third_image">
                                                      </div>
                                                      <div style="clear: both;"></div>
                                                 </td>
                                                <td class="spacer" style="border-collapse: collapse;" width="20"></td>
                                                <td class="grid_block" style="padding-top: 10px;padding-bottom: 25px;border-collapse: collapse;font-family: "Open Sans", sans-serif;" width="270" valign="top" align="left">
                                                <multiline label="Content">
                                                    <div mc:edit="block_9">
                                                      <h2 style="color: #222; font-weight: normal; font-size: 18px; margin: 0; padding: 0; margin-top: 1em; font-weight:bold;">Product Name</h2>
                                                      <p style="margin: 1em 0;">Size: <strong>'. $value['length'].'mm x '.$value['depth'].'mm </strong><br />
                                                      LEDs: <strong> '.$value['led_name'].' </strong><br />
                                                      Faces are lit: <strong>'.$value['lit_name'].'</strong><br />
                                                      Profile: <strong>'.
                                                          // set ID property of drawing to be edited
                                                          $drawing->id = $value['profile_name'];
                                                          $data = $drawing->readOne(true);
                                                          $data['name'].'
                                                      </strong><br />
                                                      Frame Colour: <strong>';
                                                        if($value['mycp'] == '#fff'){
                                                          $message .= 'White';
                                                        }elseif('#00'){
                                                          $message .= 'Black';
                                                        }else{
                                                          $message .= 'Grey';
                                                        }
                                                      $message .= '</strong><br />
                                                      Cable Exit: <strong>'.$value['cable_side'].' | Exit: '.$value['cable_exit'].' </strong>
                                                      </p>
                                                    </div>                            
                                                  </multiline>
                                                 </td>
                                                <td class="spacer" style="border-collapse: collapse;" width="20"></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div></td>
                                    </tr>';
                }
              }

            $message .= '<tr>
                          <td class="full_width" style="border-collapse: collapse; font-size:9pt;" width="100%" align="center"><div class="wrapperout" style="width: 600px;margin: auto;">
                              <table class="table600full" style="border-collapse: collapse;" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#252525">
                                <tbody>
                                  <tr class="scaleimage">
                                    <td class="spacer" style="border-collapse: collapse;" width="20"></td>
                                    <td class="grid_block" style="padding-top: 40px;padding-bottom: 40px;color: #767676;border-collapse: collapse;font-family: "Open Sans", sans-serif;;" mc:edit="footer_address" width="270" valign="middle" align="left"><multiline label="Footer Address"> &copy;2018 PIXALUX MANUFACTURING AUSTRLIA<br>
                                       </multiline></td>
                                    <td class="spacer" style="border-collapse: collapse;" width="20"></td>
                                    <td class="grid_block_nopad" style="padding-top: 40px;padding-bottom: 40px;color: #767676;border-collapse: collapse;font-family: "Open Sans", sans-serif;;" mc:edit="connect_text" width="120" valign="middle" align="right"><singleline label="Connect Text">Connected with us:</singleline></td>
                                    <td class="grid_block" style="padding-top: 40px;padding-bottom: 40px;border-collapse: collapse;" width="150" valign="middle" align="right"><multiline label="Social Link">
                                        <div mc:edit="social_link"> <a href="#" style="color: #aaaaaa;text-decoration: none;outline: none;"><img src="http://i.imgur.com/1hhkhVw.jpg" style="display: inline;vertical-align: middle;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;border: none;" alt=""></a> <a href="#" style="color: #aaaaaa;text-decoration: none;outline: none;"><img src="http://i.imgur.com/ho321ml.jpg" alt="" style="display: inline;vertical-align: middle;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;border: none;"></a> <a href="#" style="color: #aaaaaa;text-decoration: none;outline: none;"><img src="http://i.imgur.com/HeHnHVW.jpg" alt="" style="display: inline;vertical-align: middle;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;border: none;"></a> <a href="#" style="color: #aaaaaa;text-decoration: none;outline: none;"><img src="http://i.imgur.com/Q3E1tVs.jpg" alt="" style="display: inline;vertical-align: middle;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;border: none;"></a> </div>
                                      </multiline></td>
                                    <td class="spacer" style="border-collapse: collapse;" width="20"></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div></td>
                        </tr>';
            
            $message .= '<tr bgcolor="#E3E3E3">
                          <td class="full_width" style="border-collapse: collapse;" width="100%" align="center"><div class="wrapperout" style="width: 600px;margin: auto;">
                              <table class="table600con" style="border-collapse: collapse;" width="600" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                  <tr>
                                    <td class="fw_full" style="padding-top: 15px;padding-bottom: 10px;color: #666666;border-collapse: collapse;font-family: "Open Sans", sans-serif;;font-size:8pt" width="600" align="center"><multiline label="Footer Text">
                                        <div mc:edit="footer_text"> You\'re receiving this newsletter because you signed up at midnize.com<br>
                                        </div>
                                      </multiline>
                                      If you are no longer interest in receiving this email, you may
                                      <unsubscribe style="color: #000000;text-decoration: none;">unsubscribe</unsubscribe>
                                      . </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div></td>
                        </tr>';
            $message .= '<tr bgcolor="#E3E3E3">
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table>
</body>
</html>';
ini_set('sendmail_from', 'mayurvadher555@gmail.com');
ini_set('smtp_port', '25');
//add headers
$headers  = 'MIME-Version: 1.0' . "\r\n";
// $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: mayurvadher555@gmail.com' . "\r\n";
$headers = 'MIME-Version: 1.0' . "\r\n" .
           'Content-type: text/html; charset=iso-8859-1' . "\r\n" .
           'X-Mailer: PHP/' . phpversion();
echo "<pre>"; print_r($_SESSION); 
echo "<pre>"; print_r($message);
exit();
if(isset($_SESSION['client_mail'])){
  // echo "<pre>client"; print_r($_SESSION); exit();
  mail("mayur.citrusbug@gmail.com","Client Order Confirm",$message, $headers);
  $_SESSION['client_mail_send'] = '1';
  return header('location:thankyou.php');
}else{
  mail("mayur.citrusbug@gmail.com","Order Confirm",$message, $headers);
  $_SESSION['admin_mail_send'] = '1';
  // echo "<pre>admin"; print_r($_SESSION); exit();
  header('location:checkout.php');
}